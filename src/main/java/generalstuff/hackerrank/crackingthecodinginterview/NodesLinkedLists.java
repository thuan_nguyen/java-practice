package generalstuff.hackerrank.crackingthecodinginterview;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tony Nguyen on 24/05/17.
 */
public class NodesLinkedLists {
    public static class Node {
        public int data;
        public Node next;
    }

    public static boolean hasCycle(Node head) {
        Node h = head;
        Set<Node> set = new HashSet<>();
        set.add(head);
        while(h.next != null) {
            set.add(h);
            h = h.next;
            if(set.contains(h)) return true;
        }
        return false;

//        Node h = head;
//        while(h.next != null) {
//            h = h.next;
//            if(h.equals(head)) return true;
//        }
//        return false;
    }

    public static void main(String[] args) {
        Node a = new Node();
        a.data = 5;

        Node b = new Node();
        b.data = 7;

        Node c = new Node();
        c.data = 15;

        a.next = b;
        b.next = c;
        c.next = b;

        System.out.println(hasCycle(a));
    }
}
