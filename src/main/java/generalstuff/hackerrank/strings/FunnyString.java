package generalstuff.hackerrank.strings;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 29/05/17.
 */
public class FunnyString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        String[] strings = new String[n];
        String[] stringIsFunny = new String[n];

        IntStream.range(0, n).forEach(i -> {
            strings[i] = scanner.next();
            stringIsFunny[i] = stringIsFunny(strings[i]);
        });

        StringBuilder builder = new StringBuilder();
        Stream.of(stringIsFunny).forEachOrdered(string -> {
            builder.append(string);
            builder.append("\n");
        });

        System.out.println(builder.toString());
    }

    // {'a', 'b', 'c', 'd', 'e'}
    public static String stringIsFunny(String string) {
        for(int i = 1; i < string.length()/2 + 1; i++) {
            int diff1 = Math.abs(string.charAt(i) - string.charAt(i - 1));
            int diff2 = Math.abs(string.charAt(string.length() - 1 - i) - string.charAt(string.length() - i));
            if(diff1 != diff2) return "Not Funny";
        }
        return "Funny";
    }
}
