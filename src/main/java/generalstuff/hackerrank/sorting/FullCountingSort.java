package generalstuff.hackerrank.sorting;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */

public class FullCountingSort {
    public static void main(String[] args) {
        StringBuilder[] buckets = new StringBuilder[100];

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        IntStream.range(0, n).forEach(i -> {
            int x = scanner.nextInt();
            String word = scanner.next();
            if(i < n/2) word = "-";

            if(buckets[x] == null) {
                buckets[x] = new StringBuilder(word);
            } else {
                buckets[x].append(word);
            }
            buckets[x].append(" ");
        });

        StringBuilder finalBuilder = new StringBuilder();
        for(StringBuilder bucket : buckets) {
            finalBuilder.append(bucket);
        }

        System.out.println(finalBuilder.toString());
    }
}

//public class FullCountingSort {
//
//    public static void main(String[] args) {
//        Bucket[] buckets = new Bucket[100];
//
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//
//        IntStream.range(0, n).forEach(i -> {
//            int x = scanner.nextInt();
//            String word = scanner.next();
//            if(i < n/2) word = "-";
//            Node node = new Node(word);
//
//            if(buckets[x] == null) {
//                buckets[x] = new Bucket(x, node);
//            } else {
//                buckets[x].getEnd().setNext(node);
//                buckets[x].setEnd(node);
//            }
//        });
//
//        for(Bucket bucket : buckets) {
//            if (bucket != null) bucket.printAllWordsInBucket();
//        }
//    }
//
//    public static class Bucket {
//        private int number;
//        private Node start;
//        private Node end;
//
//        public Bucket(int number, Node start) {
//            this.number = number;
//            this.start = start;
//            this.end = start;
//        }
//
//        public void setEnd(Node node) {
//            this.end = node;
//        }
//
//        public Node getEnd() {
//            return end;
//        }
//
//        public void printAllWordsInBucket() {
//            Node node = start;
//            do {
//                System.out.print(node.getWord() + " ");
//                node = node.getNext();
//            } while(node != null);
//        }
//    }
//
//    public static class Node {
//        private String word;
//        private Node next;
//
//        public Node(String word) {
//            this.word = word;
//        }
//
//        public void setNext(Node node) {
//            this.next = node;
//        }
//
//        public Node getNext() {
//            return next;
//        }
//
//        public String getWord() {
//            return word;
//        }
//    }
//}


//    public static void main(String[] args) {
//        Map<Integer, List<String>> words = new HashMap<>();
//
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//
//        IntStream.range(0, n).forEach(i -> {
//            int x = scanner.nextInt();
//            String word = scanner.next();
//            if(i < n/2) word = "-";
//            if(words.containsKey(x)) {
//                words.get(x).add(word);
//            } else {
//                words.put(x, new ArrayList<>(Arrays.asList(word)));
//            }
//        });
//
//        words.values().stream().forEachOrdered(FullCountingSort::printList);
//    }

//    public static void printList(List<String> words) {
//        words.stream().forEachOrdered(word -> System.out.print(word + " "));
//    }