package strings;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class ParsingAndBasicCast {
    public static String PI = "3.14";
    public static String TEN = "10";
    public static String TRUE = "true";

    public static void main(String[] args) {
        /*** will show errors ***/
//        short myShort = Short.parseShort(PI);
//        int myInt = Integer.parseInt(PI);
//        long myLong = Long.parseLong(PI);
//
        short myShort = Short.parseShort(TEN);
        int myInt = Integer.parseInt(TEN);
        long myLong = Long.parseLong(TEN);
        System.out.println(myShort);
        System.out.println(myInt);
        System.out.println(myLong);

        float myFloat = Float.parseFloat(PI);
        double myDouble = Double.parseDouble(PI);
        System.out.println(myFloat);
        System.out.println(myDouble);

//        myLong = new Long(PI); // NumberFormatException


        myShort = (short) myFloat;
        myInt = (int) myDouble;
        myLong = (long) myDouble;
        System.out.println(myShort);
        System.out.println(myInt);
        System.out.println(myLong);


//        boolean myBoolean = Boolean.parseBoolean(TRUE);
        boolean myBoolean = Boolean.parseBoolean("TRUE");
        System.out.println(myBoolean);

        myBoolean = Boolean.parseBoolean(TRUE);
        System.out.println(myBoolean);
    }
}
