package generalstuff.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class HackerRankRadixSort {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String[] unsorted = new String[n];

        List<String> bigIntegers = new ArrayList<>();
        for(int unsorted_i=0; unsorted_i < n; unsorted_i++){
            unsorted[unsorted_i] = in.next();
            bigIntegers.add(unsorted[unsorted_i]);
        }

        bigIntegers.forEach(System.out::println);
        /* your code goes here */
        sort(bigIntegers);
        String finalInput = in.next();
        System.out.println(finalInput);
        IntStream.iterate(0, (i) -> i+1).limit(10).forEach(System.out::println);
    }

    public static void sort(List<String> list) {

        System.out.println("Substring 2: " + Integer.parseInt(Character.toString(list.get(0).charAt(2))));
    }
}
