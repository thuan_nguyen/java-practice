package generalstuff.hackerrank.bitmanipulation;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class Cipher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        String encodedMessage = scanner.next();

        String decoded = decode(encodedMessage, n, k);
        System.out.println(decoded);
    }

    public static String encode(String binary, int k) {
        int[] binaryBytes = binaryStringToIntArray(binary);
        int[] encode = new int[binary.length() + k - 1];


        for(int i = 0; i < k; i++) {
            for(int j = 0; j < binary.length(); j++) {
                encode[i + j] ^= binaryBytes[j];
            }
        }

        return intArrayToString(encode);
    }

    public static int[] binaryStringToIntArray(String binary) {
        int[] binaryArray = new int[binary.length()];

        for(int i = 0; i < binary.length(); i++) {
            binaryArray[i] = Integer.parseInt(binary.substring(i, i + 1));
        }

        return binaryArray;
    }

    public static String intArrayToString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        IntStream.of(array).forEach(i -> stringBuilder.append(i));
        return stringBuilder.toString();
    }

    public static String decode(String encoded, int k) {
        int[] binaryArray = binaryStringToIntArray(encoded);
        int[] decoded = new int[encoded.length() - k + 1];

        fillLeftHalf(binaryArray, decoded, k);

        return intArrayToString(decoded);
    }

    public static String decode(String encoded, int n, int k) {
        int lower = 0;
        int upper = n - 1;

        int[] decoded = new int[n];
        int[] cumulativeXor = new int[n];

        cumulativeXor[lower] = encoded.charAt(0) - '0';
        decoded[lower] = encoded.charAt(0) - '0';

        int range;
        for(int i = lower + 1; i <= upper; i++) {
            range = i - (k - 1) - 1;

            if(range >= 0) {
                decoded[i] ^= cumulativeXor[range];
            }

            decoded[i] ^= (cumulativeXor[i - 1] ^ (encoded.charAt(i) - '0'));
            cumulativeXor[i] = (decoded[i] ^ cumulativeXor[i - 1]);
        }
        return intArrayToString(decoded);
    }

    public static void fillLeftHalf(int[] encoded, int[] decode, int k) {
        int lower = 0;
        int upper = decode.length - 1;

        int[] cumulativeXor = new int[upper + 1];
        cumulativeXor[lower] = encoded[lower];
        decode[lower] = encoded[lower];

        int range;
        for(int i = lower + 1; i <= upper; i++) {
            range = i - (k - 1) - 1;
            if(range < 0) {
                decode[i] ^= (cumulativeXor[i - 1] ^ encoded[i]);
                cumulativeXor[i] = (decode[i] ^ cumulativeXor[i - 1]);
            } else {
                decode[i] ^= (cumulativeXor[range] ^ cumulativeXor[i - 1] ^ encoded[i]);
                cumulativeXor[i] = (decode[i] ^ cumulativeXor[i - 1]);
            }
        }
    }

    public static int xorRange(int[] array, int start, int end) {
        int val = array[start];
        for(int i = start + 1; i <= end; i++) {
            val ^= array[i];
        }
        return val;
    }
}
