package generalstuff;

import org.junit.Test;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class HackerRankBigNumbersTest {
    @Test
    public void sort_sortsNumbersProperly() {

        String[] numbers = {"6", "314159", "1", "3", "10", "3", "5"};

        List<BigInteger> bigIntegers = Stream.of(numbers).map(BigInteger::new)
                .collect(Collectors.toList());

        bigIntegers.forEach(System.out::println);

        Comparator<BigInteger> comparator = BigInteger::compareTo;

//        Sort sorter = new QuickSortHoare();
        Collections.sort(bigIntegers, comparator);
        bigIntegers.forEach(System.out::println);
    }
}