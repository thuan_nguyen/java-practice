package oop.inheritence;

/**
 * Created by Tony Nguyen on 3/06/17.
 */
public class MainOop {
    public static void main(String[] args) {
        ParentClass parent = new ParentClass();
        ParentClass parent2 = new ParentClass("parent2");

        ChildClass child = new ChildClass();
        ChildClass child2 = new ChildClass("child2");

        System.out.println("Printing stuff from parent".toUpperCase());
        parent.printName();
        parent.printNameWithGetName();
        parent.printNameWithOverride();
        parent.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from parent2".toUpperCase());
        parent2.printName();
        parent2.printNameWithGetName();
        parent2.printNameWithOverride();
        parent2.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from child".toUpperCase());
        child.printName();
        child.printNameWithGetName();
        child.printNameWithOverride();
        child.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from child2".toUpperCase());
        child2.printName();
        child2.printNameWithGetName();
        child2.printNameWithOverride();
        child2.printNameNoOverride();
        System.out.println();
    }
}
