package algorithms.fibonacci;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public class FibonacciIterativeTest extends FibonacciTest{
    @Override
    public void initializeFibonacci() {
        setFibonacci(new FibonacciIterative());
    }
}