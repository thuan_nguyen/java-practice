package generalstuff.hackerrank.implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 24/05/17.
 */
public class MigratoryBirds {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        Map<Integer, Integer> birds = new HashMap<>();

        int populationOfMostCommonBird = 0;
        for(int i = 0; i < n; i++) {
            int bird = in.nextInt();
            birds.computeIfPresent(bird, (k, v) -> v + 1);
            birds.putIfAbsent(bird, 1);
            if(birds.get(bird) > populationOfMostCommonBird) {
                populationOfMostCommonBird = birds.get(bird).intValue();
            }
        }
        int idOfCommonBird = getMinimumIdOfBirdWithCount(birds, populationOfMostCommonBird);
        System.out.println(idOfCommonBird);
    }

    public static int getMinimumIdOfBirdWithCount(Map<Integer, Integer> birds, int number) {
       return birds.entrySet().stream()
                .filter(birdType -> birdType.getValue().equals(number))
                .map(birdType -> birdType.getKey())
                .min(Integer::compareTo).get();
    }

    public static void test() {
        int[] input = {1, 4, 6, 4, 4, 3, 3, 3, 5, 6, 5, 1, 1, 3, 3, 3, 3};
        Map<Integer, Integer> birds = new HashMap<>();

        int populationOfMostCommonBird = 0;
        for(int i = 0; i < input.length; i++) {
            int bird = input[i];
            birds.computeIfPresent(bird, (k, v) -> v + 1);
            birds.putIfAbsent(bird, 1);
            if(birds.get(bird) > populationOfMostCommonBird) {
                populationOfMostCommonBird = birds.get(bird).intValue();
            }
        }
        int idOfCommonBird = getMinimumIdOfBirdWithCount(birds, populationOfMostCommonBird);
        System.out.println(idOfCommonBird);
//
//        idOfMostCommonBird(birds);
//        getMinimumIdOfBirdWithCount(birds, 2);
    }
}