package generalstuff.hackerrank.crackingthecodinginterview;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class TriesContacts {
    public static void main(String[] args) {
        System.out.println();
//        new ContactsAppSimulator().start();
        ConsoleContactsApp consoleApp = new ConsoleContactsApp();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int a0 = 0; a0 < n; a0++){
            String op = in.next();
            String contact = in.next();
            consoleApp.processCommand(op, contact);
        }
    }

    public static class ContactsAppSimulator {
        private ConsoleContactsApp app;

        public ContactsAppSimulator() {
            final String[] contacts = {"bob", "bobcat", "bobby", "alice", "alex"};
            app = new ConsoleContactsApp(contacts);
        }

        public ContactsAppSimulator(String[] contacts) {
            app = new ConsoleContactsApp(contacts);
        }

        public void start(){
            app.processCommand("add", "hack");
            app.processCommand("add", "hackerrank");
            app.processCommand("find", "hac");
            app.processCommand("find", "");
            app.processCommand("find", "ale");
        }
    }

    public static class ConsoleContactsApp {
        private final Contacts contacts;

        public ConsoleContactsApp() {
            contacts = new Contacts();
        }

        public ConsoleContactsApp(String[] contacts) {
            this.contacts = new Contacts(contacts);
        }

        public void processCommand(String operation, String arg) {
            if(operation.equals("add")) {
                contacts.addContact(arg);
            }

            if(operation.equals("find")) {
                int numberOfResults = contacts.numberOfPossibleResults(arg);
                System.out.println(numberOfResults);
            }
        }
    }

    public static class Contacts {
        private final Map contacts;

        public Contacts() {
            contacts = new HashMap<>();
        }

        public Contacts(String[] contacts) {
            this.contacts = new HashMap<>();
            Stream.of(contacts).forEach((name) -> addContact(name));
        }

        public void addContact(String name) {
            addContact(contacts, name, 0);
        }

        private void addContact(Map map, String name, int index) {
            map.putIfAbsent(name.charAt(index), new HashMap<>());
            map.computeIfPresent("count", (k, v) -> (int)v + 1);
            map.putIfAbsent("count", 1);
            if((name.length() - 1) - index > 0) {
//                map.computeIfPresent("count", (k, v) -> 1);
                addContact((Map)map.get(name.charAt(index)), name, ++index);
            } else {
                Map end = (Map)map.get(name.charAt(index));
                ((Map)map.get(name.charAt(index))).put(null, null);
                end.computeIfPresent("count", (k, v) -> (int)v + 1);
                end.putIfAbsent("count", 1);
            }
        }

        public int numberOfPossibleResults(String name) {
            Map contactsNode = this.contacts;
            for(int i = 0; i < name.length(); i++) {
                contactsNode = (Map)contactsNode.get(name.charAt(i));
                if(contactsNode == null) return 0;
            }
            return (int)contactsNode.get("count");
        }
    }
}


//        Contacts contacts = new Contacts();
//        contacts.addContact("bob");
//        contacts.addContact("bobcat");
//        contacts.addContact("alice");
//        contacts.numberOfPossibleResults("bob");

//        public Stream<String> searchContactStream(String name) {
//            String pattern = "^" + name + ".*";
//            Pattern p = Pattern.compile(pattern);
//            return contacts.stream()
//                    .filter((e) -> p.matcher(e).matches());
//        }