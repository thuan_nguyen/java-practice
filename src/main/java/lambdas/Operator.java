package lambdas;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public interface Operator<T> {
    T operate(T a, T b);
}
