package algorithms.binaryindextree;

import java.io.Serializable;

/**
 * Author: Tony Nguyen on 5/06/17.
 */
public interface BinaryIndexTree<T> extends Serializable {
    T fold(int end);
    T foldRange(int start, int end);

    void update(int index, T value);

    T getPointInOriginal(int index);
    T getPointInBit(int index);

    int length();

    String cumulativeString();

    String bitToString();

//    String toString();
}
