package generalstuff;

import java.util.*;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class CsgProblem1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String firstLine = scanner.nextLine();

        String secondLine = scanner.nextLine();

        boolean result = checkAllMatch(firstLine, secondLine);

        String message = result ? "YES" : "NO";
        System.out.println(message);
    }

    public static boolean checkAllMatch(String[] firstSetOfNumbers, String[] secondSetOfNumbers) {
        List<String> first = new ArrayList<>(Arrays.asList(firstSetOfNumbers));
        List<String> second = new ArrayList<>(Arrays.asList(secondSetOfNumbers));

        return first.containsAll(second);
    }

    public static boolean checkAllMatch(String firstSetOfNumbers, String secondSetOfNumbers) {
        return checkAllMatch(firstSetOfNumbers.split(" "), secondSetOfNumbers.split(" "));
    }
}
