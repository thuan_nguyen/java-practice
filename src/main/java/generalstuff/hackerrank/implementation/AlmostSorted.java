package generalstuff.hackerrank.implementation;

import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class AlmostSorted {
    public static void main(String[] args) {
        int[] arr = new int[2];
    }

    public static int[] reversableSequence(int[] array) {
        int[] sortIndices = new int[2];
        boolean reversableStart = false;
        for(int i = 0; i < array.length - 1; i++) {
            int point = array[i];
            if (point > array[i + 1]) {
                if(!reversableStart) {
                    reversableStart = true;
                    sortIndices[0] = i + 1;
                } else {
                    if(i + 1 == array.length - 1) {
                        sortIndices[1] = i + 1;
                        break;
                    }
                }
            } else if (reversableStart) {
                sortIndices[1] = i + 1;
                break;
            }
        }
        return sortIndices;
    }

    public static void printMessage(int[] array, int[] sortIndices) {
        if(sortIndices == null) {
            System.out.println("no");
            return;
        }

        if((sortIndices[0] - sortIndices[1]) > 2) {
            if(array[sortIndices[1] - 2] < array[sortIndices[0]]) {
                System.out.println("yes");
                System.out.print("reverse ");
                printArray(sortIndices);
                return;
            }
        }

        System.out.println("yes");
        System.out.print("reverse ");
        printArray(sortIndices);
    }

    public static void printArray(int[] array) {
        IntStream.range(0, array.length - 1)
                .forEach(e -> System.out.print(array[e] + " "));
        System.out.println(array[array.length - 1]);
    }
}
