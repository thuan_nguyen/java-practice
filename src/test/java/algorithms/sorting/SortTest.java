package algorithms.sorting;

import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public abstract class SortTest {
    private Sort SORTER;

    protected Sort getSorter() {
        return SORTER;
    }

    protected void setup(Sort provider) {
        this.SORTER = provider;
    }

    @Before
    public abstract void initializeSorter();

    @Test
    public void sort_canSortReversedValues() {
        int[] INPUT = {5, 4, 3, 2, 1};
        int[] EXPECTED = {1, 2, 3, 4, 5};

        List<Integer> input = IntStream.of(INPUT).boxed()
                .collect(Collectors.toList());

        List<Integer> expected = IntStream.of(EXPECTED).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compareTo;

        SORTER.sort(input, comparator);

        IntStream.range(0, input.size())
                .forEach((i) -> assertThat(input.get(i)).isEqualTo(expected.get(i)));
    }

    @Test
    public void reverse_canReverseOrder() {
        int[] INPUT = {1, 2, 3, 4, 5};
        int[] EXPECTED = {5, 4, 3, 2, 1};

        List<Integer> input = IntStream.of(INPUT).boxed()
                .collect(Collectors.toList());

        List<Integer> expected = IntStream.of(EXPECTED).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compareTo;

        SORTER.reverse(input, comparator);

        assertThat(input).containsExactly(IntStream.of(EXPECTED)
                .boxed()
                .toArray(Integer[]::new)
        );
    }

    @Test
    public void sort_canSortRandomSmallList() {
        int[] INPUT = {2, 5, 1, 3, 4};
        int[] EXPECTED = {1, 2, 3, 4, 5};

        List<Integer> input = IntStream.of(INPUT).boxed()
                .collect(Collectors.toList());

        List<Integer> expected = IntStream.of(EXPECTED).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compareTo;

        SORTER.sort(input, comparator);

        IntStream.range(0, input.size())
                .forEach((i) -> assertThat(input.get(i))
                        .isEqualTo(expected.get(i))
                );
    }

    @Test
    public void sort_canSortRandomSmallList_BiggerNumbers() {
        int[] INPUT = {6, 314159, 1, 3, 10, 3, 5};
        int[] EXPECTED = {1, 3, 3, 5, 6, 10, 314159};

        List<Integer> input = IntStream.of(INPUT).boxed()
                .collect(Collectors.toList());

        List<Integer> expected = IntStream.of(EXPECTED).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compareTo;

        SORTER.sort(input, comparator);

        IntStream.range(0, input.size())
                .forEach((i) -> assertThat(input.get(i))
                        .isEqualTo(expected.get(i))
                );
    }

//    String[] numbers = {"6", "314159", "1", "3", "10", "3", "5"};
}