package oop.inheritence;

/**
 * Created by Tony Nguyen on 3/06/17.
 */
public class MainOopMixCast {
    public static void main(String[] args) {
        ParentClass parent = new ChildClass();
        ParentClass parent2 = new ChildClass("child - parent2");

        System.out.println(parent.getName());
        System.out.println(parent2.getName());

        ChildClass childCast1 = new ChildClass();
        ChildClass childCast2 = new ChildClass("child2");

        //// OUTPUTS ERROR - ClassCastException
//        ChildClass parentCast1 = (ChildClass) new ParentClass();
//        ChildClass parentCast2 = (ChildClass) new ParentClass();

        System.out.println("Printing stuff from parent".toUpperCase());
        parent.printName();
        parent.printNameWithGetName();
        parent.printNameWithOverride();
        parent.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from parent2".toUpperCase());
        parent2.printName();
        parent2.printNameWithGetName();
        parent2.printNameWithOverride();
        parent2.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from childCast1".toUpperCase());
        childCast1.printName();
        childCast1.printNameWithGetName();
        childCast1.printNameWithOverride();
        childCast1.printNameNoOverride();
        System.out.println();

        System.out.println("Printing stuff from childCast2".toUpperCase());
        childCast2.printName();
        childCast2.printNameWithGetName();
        childCast2.printNameWithOverride();
        childCast2.printNameNoOverride();
        System.out.println();

//        System.out.println("Printing stuff from parentCast1".toUpperCase());
//        parentCast1.printName();
//        parentCast1.printNameWithGetName();
//        parentCast1.printNameWithOverride();
//        parentCast1.printNameNoOverride();
//        System.out.println();
//
//        System.out.println("Printing stuff from parentCast2".toUpperCase());
//        parentCast2.printName();
//        parentCast2.printNameWithGetName();
//        parentCast2.printNameWithOverride();
//        parentCast2.printNameNoOverride();
//        System.out.println();
    }
}
