package strings;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class Strings {
    public static void main(String[] args) {
        final String MY_STRING = "my string";
        String stringConcat = MY_STRING + " concat";

        StringBuilder stringBuilder = new StringBuilder("Hello world");
        stringBuilder.append("!");

        System.out.println(stringConcat + 5 + 10 + (5 + 10));
        System.out.println(stringBuilder.toString());
    }
}
