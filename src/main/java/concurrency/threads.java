package concurrency;

import algorithms.fibonacci.FibonacciIterative;
import algorithms.fibonacci.FibonacciRecursive;

import static java.lang.Thread.currentThread;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public class threads {
    public static void main(String[] args) {
        System.out.println("Inside main");

        final int N = 15;
        System.out.println("Testing fibonacci N = " + N + "\n");

        Thread thread1 = threadOne(N);
        thread1.setName("thread1");

        Thread thread2 = threadTwo(N);
        thread2.setName("thread2");

        System.out.println(thread2.getName() + " started");
        thread2.start();
        System.out.println();

        System.out.println(thread1.getName() + " started");
        thread1.start();
        System.out.println();

    }

    public static Thread threadOne(int n) {
        Thread thread = new Thread(() -> {
            int fibonacci = fibonacciForLoop(n);
            String threadName = currentThread().getName();

            System.out.println(currentThread().getName()
                    + " - "
                    + "FibonacciIterative For Loop style:\n"
                    + fibonacci
                    + "\n"
            );
        });
        return thread;
    }

    public static Thread threadTwo(int n) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int fibonacci = fibonacciRecursive(n);

                System.out.println(currentThread().getName()
                        + " - "
                        + "FibonacciIterative Recursive style:\n"
                        + fibonacci
                        + "\n"
                );
            }
        });
        return thread;
    }

    public static int fibonacciForLoop(int n) {
        return new FibonacciIterative().fibonacci(n);
    }

    public static int fibonacciRecursive(int n) {
        return new FibonacciRecursive().fibonacci(n);
    }
}
