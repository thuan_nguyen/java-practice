package algorithms.binaryindextree;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class BitMain {
    public static void main(String[] args) throws Exception {
//        BinaryIndexTrees bit = new BinaryIndexTrees(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
        try {
//            testSums();
            testXors();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void testSums() throws Exception {
        BinaryIndexTrees bit = new BinaryIndexTrees(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
        bit.printOriginal();
        bit.printBitXor();
        bit.printBitSum();

        System.out.println("\nLINEAR SUM:");
        System.out.print(bit.sumTotal(0) + ", ");
        System.out.print(bit.sumTotal(1) + ", ");
        System.out.print(bit.sumTotal(2) + ", ");
        System.out.print(bit.sumTotal(3) + ", ");
        System.out.print(bit.sumTotal(4) + ", ");
        System.out.print(bit.sumTotal(5) + ", ");
        System.out.print(bit.sumTotal(6) + ", ");
        System.out.print(bit.sumTotal(7) + ", ");
        System.out.print(bit.sumTotal(8) + ", ");
        System.out.println();

        int lower = 5, upper = 7;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));

        lower = 0; upper = 1;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));

        lower = 0; upper = 0;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));

        lower = 1; upper = 2;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));

        lower = 0; upper = 5;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));

        lower = 0; upper = 11;
        System.out.println("\nSUM RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.sumRange(lower, upper));
    }

    public static void testXors() throws Exception {
        BinaryIndexTrees bit = new BinaryIndexTrees(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
        bit.printOriginal();
        bit.printBitXor();
        bit.printBitSum();

        int lower = 5, upper = 7;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));

        lower = 0; upper = 1;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));

        lower = 0; upper = 0;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));

        lower = 1; upper = 2;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));

        lower = 0; upper = 5;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));

        lower = 0; upper = 11;
        System.out.println("\nXOR RANGE (" + lower + ", " + upper + "):");
        System.out.println(bit.xorRange(lower, upper));
    }
}
