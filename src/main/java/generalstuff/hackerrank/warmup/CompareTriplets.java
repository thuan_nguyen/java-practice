package generalstuff.hackerrank.warmup;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class CompareTriplets {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a0 = in.nextInt();
        int a1 = in.nextInt();
        int a2 = in.nextInt();
        int b0 = in.nextInt();
        int b1 = in.nextInt();
        int b2 = in.nextInt();
        int[] result = solve(a0, a1, a2, b0, b1, b2);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");
    }


    static int[] solve(int a0, int a1, int a2, int b0, int b1, int b2){
        final int ALICE = 0;
        final int BOB = 1;
        int[] aliceScores = new int[]{a0, a1, a2};
        int[] bobScores = new int[]{b0, b1, b2};

        int[] result = new int[]{0, 0};

        for(int i = 0; i < aliceScores.length; i++) {
            int comparison = Integer.compare(aliceScores[i], bobScores[i]);
            if (comparison > 0) {
                result[ALICE] = result[ALICE] + 1;
            } else if(comparison < 0) {
                result[BOB] = result[BOB] + 1;
            }
        }

        return result;
    }
}
