package algorithms.fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public final class FibonacciIterative implements Fibonacci {
    @Override
    public int fibonacci(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;

        int a = 0;
        int b = 1;
        int result = 0;
        for (int i = 2; i <= n; i++) {
            result = a + b;
            a = b;
            b = result;
        }
        return result;
    }

    @Override
    public List<Integer> fibonacciList(int n) {
        List<Integer> fibonacciNumbers = new ArrayList<>();
        fibonacciNumbers.add(0);
        if (n <= 0) return fibonacciNumbers;

        fibonacciNumbers.add(1);
        if (n == 1) return fibonacciNumbers;

        int a = 0;
        int b = 1;
        int result;
        for (int i = 2; i <= n; i++) {
            result = a + b;
            fibonacciNumbers.add(result);
            a = b;
            b = result;
        }
        return fibonacciNumbers;
    }
}
