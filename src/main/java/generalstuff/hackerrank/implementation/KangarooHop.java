package generalstuff.hackerrank.implementation;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class KangarooHop {
    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int x1 = in.nextInt();
//        int v1 = in.nextInt();
//        int x2 = in.nextInt();
//        int v2 = in.nextInt();
        int x1 = 0, v1 = 3;
        int x2 = 4, v2 = 2;

        RacingKangaroo k1 = new RacingKangaroo(x1, v1, 0);
        RacingKangaroo k2 = new RacingKangaroo(x2, v2, 1);

        simulate(k1, k2);

        String kangaroosWillMeet = kangaroosWillMeet(k1, k2);
        System.out.println(kangaroosWillMeet);
    }

    public static void simulate(RacingKangaroo k1, RacingKangaroo k2) {
        while ((k1.getPosition() != k2.getPosition())
                && (k1.getPosition() < k2.getPosition())) {
            k1.jump();
            k2.jump();
        }
    }

    public static String kangaroosWillMeet(RacingKangaroo k1, RacingKangaroo k2) {
        return k1.getPosition() == k2.getPosition() ? "YES" : "NO";
    }

    public static class RacingKangaroo implements Comparable<RacingKangaroo> {
        private int position;
        private int leapDistance;
        private int id;

        public RacingKangaroo(int position, int leapDistance, int id) {
            this.position = position;
            this.leapDistance = leapDistance;
            this.id = id;
        }

        public void jump() {
            position += leapDistance;
        }

        public int getLeapDistance() {
            return leapDistance;
        }

        public int getPosition() {
            return position;
        }

        public int getId() {
            return id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RacingKangaroo)) return false;

            RacingKangaroo kangaroo = (RacingKangaroo) o;

            if (position != kangaroo.position) return false;
            if (leapDistance != kangaroo.leapDistance) return false;
            return id == kangaroo.id;
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public int compareTo(RacingKangaroo kangaroo) {
            return Integer.compare(this.getPosition(), kangaroo.getPosition());
        }

        @Override
        public String toString() {
            return "{" +
                    "\"id\":" + id +
                    ", \"position\":" + position +
                    ", \"leapDistance\":" + leapDistance +
                    '}';
        }
    }
}

