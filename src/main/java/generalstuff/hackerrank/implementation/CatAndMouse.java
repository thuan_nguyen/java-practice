package generalstuff.hackerrank.implementation;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class CatAndMouse {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for(int a0 = 0; a0 < q; a0++){
            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
            String result = evaluateScenario(x, y, z);
            System.out.println(result);
        }
    }

    public static String evaluateScenario(int catA, int catB, int mouseC) {
        int distanceCatA = distanceFromMouse(catA, mouseC);
        int distanceCatB = distanceFromMouse(catB, mouseC);

        if(distanceCatA < distanceCatB) return "Cat A";
        if(distanceCatA > distanceCatB) return "Cat B";
        return "Mouse C";
    }

    public static int distanceFromMouse(int cat, int mouse) {
        return Math.abs(mouse - cat);
    }
}
