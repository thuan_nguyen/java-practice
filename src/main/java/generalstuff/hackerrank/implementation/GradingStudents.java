package generalstuff.hackerrank.implementation;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class GradingStudents {

    static int[] solve(int[] grades){
        // Complete this function
        return calculateFinalGrade(grades);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] grades = new int[n];
        for(int grades_i=0; grades_i < n; grades_i++){
            grades[grades_i] = in.nextInt();
        }
        int[] result = solve(grades);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? "\n" : ""));
        }
    }

    public static int[] calculateFinalGrade(int[] grades) {
        return IntStream.of(grades)
                .boxed()
                .map(GradingStudents::roundGrade)
                .mapToInt(i -> i).toArray();
    }

    public static int roundGrade(int grade) {
        if(isFailingGrade(grade)) return grade;

        int remainder = grade % 5;
        if (remainder >= 3) return grade + (5 - remainder);

        return grade;
    }

    public static boolean isFailingGrade(int grade) {
        final int LIMIT_ROUND_POINT = 38;
        return grade < LIMIT_ROUND_POINT;
    }
}
