package generalstuff.hackerrank;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class HackerRankBigNumbers {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String[] unsorted = new String[n];

        List<BigInteger> bigIntegers = new ArrayList<>();
        for(int unsorted_i=0; unsorted_i < n; unsorted_i++){
            unsorted[unsorted_i] = in.next();
            bigIntegers.add(new BigInteger(unsorted[unsorted_i]));
        }

        Comparator<BigInteger> comparator = BigInteger::compareTo;

        sort(bigIntegers, comparator);
        bigIntegers.forEach(System.out::println);
        /* your code goes here */

        String finalInput = in.next();
        System.out.println(finalInput);
    }


    public static void sort(List list, Comparator comparator) {
        quicksort(list, 0, list.size() - 1, comparator);
    }

    public static <T> void quicksort(List<T> list, int low, int high, Comparator<T> comparator) {
        if (low < high) {
            int pivot = partition(list, low, high, comparator);
            quicksort(list, low, pivot - 1, comparator);
            quicksort(list, pivot, high, comparator);
        }
    }

    private static void swap(List list, int p1, int p2) {
        Object val1 = list.get(p1);
        Object val2 = list.get(p2);

        list.set(p1, val2);
        list.set(p2, val1);
    }

    public static int partition(List list, int low, int high, Comparator comparator) {
        int midIndex = (low + high)/2;
        Object midItem = list.get(midIndex);
        while(low <= high) {
            while (comparator.compare(list.get(low), midItem) < 0) {
                ++low;
            }

            while (comparator.compare(list.get(high), midItem) > 0) {
                --high;
            }

            if(low <= high) {
                swap(list, low, high);
                ++low;
                --high;
            }
        }

        return low;
    }
}
