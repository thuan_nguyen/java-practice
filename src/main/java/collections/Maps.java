package collections;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tony Nguyen on 19/05/17.
 */
public class Maps {
    public static void main(String[] args) {
        Map<String, Integer> basicMap = basicHashMap();
        System.out.println(basicMap.get("one"));
        System.out.println(basicMap.putIfAbsent("one", 2));
        System.out.println(basicMap.get("one"));
        System.out.println(basicMap.put("one", 2));
        System.out.println(basicMap.get("one"));
        System.out.println(basicMap.putIfAbsent("four", 4));
        System.out.println(basicMap.get("four"));

        System.out.println("\n");
        basicMap.forEach(((s, integer) -> {
            System.out.println(s);
            System.out.println(integer);
        }));

        System.out.println(basicMap.get("four"));

        for(Map.Entry<String, Integer> entry : basicMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }

    public static Map<String, Integer> basicHashMap() {
        Map<String, Integer> basicMap = new HashMap<>();
        basicMap.put("one", 1);
        basicMap.put("two", 2);
        basicMap.put("three", 3);
        return basicMap;
    }
}
