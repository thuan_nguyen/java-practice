package generalstuff.hackerrank.sorting;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class CountingSort2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] numbers = new int[100];
        IntStream.range(0, n).forEach(i -> numbers[scanner.nextInt()]++);

        IntStream.range(0, numbers.length).forEach(i -> printNumbers(i, numbers[i]));
    }

    public static void printNumbers(int number, int iterations) {
        IntStream.range(0, iterations).forEach(i -> System.out.print(number + " "));
    }
}
