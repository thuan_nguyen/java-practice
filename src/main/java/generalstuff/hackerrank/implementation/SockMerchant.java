package generalstuff.hackerrank.implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class SockMerchant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Map<Integer, Integer> socks = new HashMap<>();

        IntStream.range(0, n).forEach(i -> {
            int sock = scanner.nextInt();
            socks.computeIfPresent(sock, (k, v) -> v + 1);
            socks.putIfAbsent(sock, 1);
        });

        int matchingPairs = computeNumberMatchingPairs(socks);

        System.out.println(matchingPairs);
    }

    public static int computeNumberMatchingPairs(Map<Integer, Integer> socks) {
        return socks.values().stream()
                .map(sock -> sock % 2 > 0 ? (sock - 1)/2 : sock/2)
                .reduce(Integer::sum).get();
    }
}
