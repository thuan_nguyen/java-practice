package generalstuff.hackerrank.strings;

/**
 * Created by Tony Nguyen on 24/05/17.
 */
public class MarsExploration {
    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        String message = in.next();

        String INPUT = "SOSSPSSQSSOR";
        String EXPECTED = "SOS";
        int numberOfAlteredLetters = numberOfAlteredLetters(INPUT, EXPECTED);
        System.out.println(numberOfAlteredLetters);
    }

    public static int numberOfAlteredLetters(String message, String expected) {
        int limit = Math.min(message.length(), expected.length());
        int nonMatchingCharacters = 0;
        for(int i = (expected.length() - 1); i < message.length(); i += expected.length()) {
            nonMatchingCharacters += numberOfNonMatchingCharacters(
                    message.substring(i - (expected.length() - 1), i + 1), expected);
        }
        return nonMatchingCharacters;
    }

    public static int numberOfNonMatchingCharacters(String first, String second) {
        int limit = Math.min(first.length(), second.length());
        int nonMatchingCharacters = Math.abs(first.length() - second.length());
        for(int i = 0; i < limit; i++) {
            if(first.charAt(i) != second.charAt(i)) ++nonMatchingCharacters;
        }
        return nonMatchingCharacters;
    }
}
