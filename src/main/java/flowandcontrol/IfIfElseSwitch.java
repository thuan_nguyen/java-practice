package flowandcontrol;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class IfIfElseSwitch {
    public static void main(String[] args) {
        final int TEST_NUMBER = 5;
        System.out.println(lessThanTen(TEST_NUMBER));
        System.out.println(lessThanTen(11));

        System.out.println(lessThanTenAndGreaterThan4(TEST_NUMBER));
        System.out.println(lessThanTenAndGreaterThan4(4));

        switching(1);
        switching(2);
        switching(3);
    }

    public static void switching(int number) {
        switch (number) {
            case 1:
                System.out.println("number is 1");
                System.out.println("Hello World!");
                break;
            case 2:
                System.out.println("number is 2");
                System.out.println("Goodbye World!");
                break;
            default:
                System.out.println("number is not 1 or 2");
                System.out.println("BINGO!");
                break;
        }
    }

    public static boolean lessThanTen(int number) {
        if (number < 10) {
            System.out.println("Number is less than 10");
            return true;
        } else {
            System.out.println("Number is 10 or greater");
            return false;
        }
    }

    public static boolean lessThanTenAndGreaterThan4(int number) {
        if (number < 10 && number > 4){
            System.out.println("Number is less than 10 and greater than 4");
            return true;
        }
        System.out.println("Number is 4 or less, or 10 or greater");
        return false;
    }
}
