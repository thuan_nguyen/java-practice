package oop.abstractclasses;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public abstract class Book {
    String title;
    String author;

    Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    abstract void display();
}
