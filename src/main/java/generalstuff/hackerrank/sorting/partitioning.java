package generalstuff.hackerrank.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class partitioning {
    public static int[] partition(int[] array, int lower, int upper) {
        int pivot = array[0];
        List<Integer> leftPartition = new ArrayList<>();
        List<Integer> equalPartition = new ArrayList<>();
        List<Integer> rightPartition = new ArrayList<>();

        for(int element : array) {
            if(element < pivot) leftPartition.add(element);
            else if(element > pivot) rightPartition.add(element);
            else equalPartition.add(element);
        }

        leftPartition.addAll(equalPartition);
        leftPartition.addAll(rightPartition);

        int[] partition = leftPartition.stream().mapToInt(i -> i).toArray();
        return partition;
    }

    public static void swap(int[] array, int p1, int p2) {
        int temp = array[p1];
        array[p1] = array[p2];
        array[p2] = temp;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println(array[array.length - 1]);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] array = new int[n];
        IntStream.range(0, n).forEach(i -> array[i] = scanner.nextInt());


        int[] partition = partition(array, 0, array.length - 1);
        printArray(partition);
    }

}
