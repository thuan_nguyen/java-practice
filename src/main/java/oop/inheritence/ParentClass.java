package oop.inheritence;

/**
 * Created by Tony Nguyen on 3/06/17.
 */
public class ParentClass {
    private String name = "parent";

    public ParentClass() {

    }

    public ParentClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void printName() {
        System.out.println("ParentClass: printName");
        System.out.println(this.name);
    }

    public void printNameWithGetName() {
        System.out.println("ParentClass: printNameWithGetName");
        System.out.println(this.getName());
    }

    public void printNameNoOverride() {
        System.out.println("ParentClass: printNameNoOverride");
        System.out.println(this.name);
    }

    public void printNameWithOverride() {
        System.out.println("ParentClass: printNameWithOverride");
        System.out.println(this.name);
    }
}
