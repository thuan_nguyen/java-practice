package generalstuff.hackerrank.implementation;

import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 24/05/17.
 */
public class EqualizeArray {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        int[] array = new int[n];
//        IntStream.range(0, n).forEach((i) -> array[i] = scanner.nextInt());
//
//        Map<Integer, Integer> numberOccurrences = new HashMap<>();
//        int max = 0;
//
//        for(int number : array) {
//            numberOccurrences.computeIfPresent(number, (k, v) -> v + 1);
//            numberOccurrences.putIfAbsent(number, 1);
//            if(numberOccurrences.get(number) > max) max = numberOccurrences.get(number);
//        }
//
//        int minDeletions = array.length - max;
//
//        System.out.println(minDeletions);

        int[] numbs = {1, 2, 3, 4};
        System.out.println(IntStream.of(numbs).sum());
    }
}
