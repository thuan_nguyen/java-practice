package generalstuff.hackerrank.strings;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 24/05/17.
 */
public class CaesarCipher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String unencrypted = scanner.next();
        int key = scanner.nextInt();

        String cipher = createCipher(unencrypted, key);
        System.out.println(cipher);
    }

    public static char nextChar(char base, int key) {
        char nextChar = base;
        if((base >= 'A') && (base <= 'Z')) {
            nextChar = (char)((base - 'A' + key) % (26) + 'A');
        } else if ((base >= 'a') && (base <= 'z')) {
            nextChar = (char)((base - 'a' + key) % (26) + 'a');
        }
        return nextChar;
    }

    public static String createCipher(String value, int key) {
        char[] cipher = new char[value.length()];
        for(int i = 0; i < value.length(); i++) {
            char letter = value.charAt(i);
            cipher[i] = nextChar(letter, key);
        }
        return new String(cipher);
    }
}
