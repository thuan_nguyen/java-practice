package generalstuff.hackerrank.implementation;

import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class MagicSquare {
    public static class Simulation {
//        private final int[][] matrix = {{1, 2, 3, 4},
//                                {4, 5, 6, 7},
//                                {7, 8, 9, 10}};

//        private final int[][] matrix = {{1, 2, 3},
//                                        {4, 5, 6},
//                                        {7, 8, 9}};

        private final int[][] matrix = {{4, 9, 2},
                                        {3, 5, 7},
                                        {8, 1, 5}};

        public void start() {
            System.out.println(matrix.length);
            System.out.println(matrix[0].length);
            System.out.println("Matrix:");
            MagicSquare.printMatrix(matrix);
            System.out.println();

            System.out.println("Sum Rows:");
            printArray(MagicSquare.sumRows(matrix));
            System.out.println();

            System.out.println("Sum Columns:");
            printArray(MagicSquare.sumColumns(matrix));
            System.out.println();

            System.out.println("Sum Diagonals:");
            try { printArray(MagicSquare.sumDiagonals(matrix)); }
            catch (Exception e) { System.out.println(e);}
            System.out.println();
        }
    }

    public static void main(String[] args) {
        new Simulation().start();
//        Scanner in = new Scanner(System.in);
//        int[][] s = new int[3][3];
//        for(int s_i=0; s_i < 3; s_i++){
//            for(int s_j=0; s_j < 3; s_j++){
//                s[s_i][s_j] = in.nextInt();
//            }
//        }
        //  Print the minimum cost of converting 's' into a magic square
    }

    public static int[] sumColumns(int[][] matrix) {
        int[] sums = new int[matrix.length];
        for(int row = 0; row < sums.length; row++) {
            for(int col = 0; col < matrix[0].length; col++) {
                sums[row] = sums[row] + matrix[row][col];
            }
        }
        return sums;
    }

    public static int[] sumRows(int[][] matrix) {
        int[] sums = new int[matrix[0].length];
        for(int col = 0; col < sums.length; col++) {
            for(int row = 0; row < matrix.length; row++) {
                sums[col] = sums[col] + matrix[row][col];
            }
        }
        return sums;
    }

    public static int[] sumDiagonals(int[][] matrix) throws Exception {
        if(matrix.length != matrix[0].length) throw new Exception("matrix not square".toUpperCase());
        int[] diagonals = new int[2];

        for(int i = 0; i < matrix.length; i++) {
            diagonals[0] = diagonals[0] + matrix[i][i];
            diagonals[1] = diagonals[1] + matrix[matrix.length - 1 - i][i];
        }
        return diagonals;
    }
    public static void printArray(int[] array) {
        IntStream.range(0, array.length - 1)
                .forEach(e -> System.out.print(array[e] + " "));
        System.out.println(array[array.length - 1]);
    }

    public static void printMatrix(int[][] matrix) {
        IntStream.range(0, matrix.length)
                .forEach(i -> printArray(matrix[i]));
    }
}
