package generalstuff.hackerrank.sorting;

import java.util.*;

/**
 * Created by Tony Nguyen on 28/05/17.
 */
public class BigSorting {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<String> numbers = new ArrayList<>();
        for(int unsorted_i=0; unsorted_i < n; unsorted_i++){
            numbers.add(in.next());
        }

        Comparator<String> comparator = (o, t1) -> {
            if(o.length() != t1.length()) return o.length() - t1.length();
            else return o.compareTo(t1);
        };

        Collections.sort(numbers, comparator);
        StringBuilder builder = new StringBuilder();

        numbers.forEach(number -> {
            builder.append(number);
            builder.append("\n");
        });

        System.out.println(builder.toString());
    }
}
