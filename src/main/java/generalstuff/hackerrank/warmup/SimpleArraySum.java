package generalstuff.hackerrank.warmup;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class SimpleArraySum {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            numbers.add(scanner.nextInt());
        }
        int sum = numbers.stream().reduce(0, Integer::sum);
        System.out.println(sum);
    }
}
