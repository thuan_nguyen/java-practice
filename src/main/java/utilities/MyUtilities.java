package utilities;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Author: Tony Nguyen on 4/06/17.
 */
public class MyUtilities {
    public static void printIntArray(int[] array) {
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < array.length - 1; i++) {
            output.append(array[i]);
            output.append(", ");
        }

        output.append(array[array.length - 1]);
        System.out.println(output.toString());
    }

    public static <T extends Object> String arrayToString(T[] array) {
        StringBuilder builder = new StringBuilder();
        IntStream.range(0, array.length - 1).forEach(i -> {
            builder.append(array[i]).append(", ");
        });

        builder.append(array[array.length - 1]);
        return builder.toString();
    }

    public static <T> String listToString(List<T> list) {
        StringBuilder builder = new StringBuilder();
        IntStream.range(0, list.size() - 1).forEach(i -> {
            builder.append(list.get(i)).append(", ");
        });

        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }
}
