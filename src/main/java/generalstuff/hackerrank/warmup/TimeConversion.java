package generalstuff.hackerrank.warmup;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class TimeConversion {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String time = scanner.nextLine();

        String output = convertTo24Hour(time);
        System.out.println(output);
    }

    public static String convertTo24Hour(String time) throws DateTimeParseException {
        final DateTimeFormatter INPUT_FORMAT = DateTimeFormatter.ofPattern("hh:mm:ssa");
        final DateTimeFormatter OUTPUT_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss");

        LocalTime localTime = LocalTime.parse(time, INPUT_FORMAT);
        String output = localTime.format(OUTPUT_FORMAT);
        return output;
    }
}
