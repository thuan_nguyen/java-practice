package generalstuff.hackerrank.sorting;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class Quicksort3 {
    public static void quicksort(int[] array, int lower, int upper) {
        if(lower < upper) {
            int pivot = partition(array, lower, upper);
            printArray(array);
            quicksort(array, lower, pivot - 1);
            quicksort(array, pivot+1, upper);
        }
    }

    public static int partition(int[] array, int lower, int upper){
        int pivot = array[upper];
        int i = lower - 1;
        for(int j = lower; j < upper; j++) {
            if(array[j] < pivot) {
                ++i;
                if(i != j) {
                    swap(array, i, j);
                }
            }
        }
        swap(array, i + 1, upper);
        return i + 1;
    }

    public static void swap(int[] array, int p1, int p2) {
        int temp = array[p1];
        array[p1] = array[p2];
        array[p2] = temp;
    }

    public static void printArray(int[] array) {
        IntStream.range(0, array.length - 1)
                .forEach(e -> System.out.print(array[e] + " "));
        System.out.println(array[array.length - 1]);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] numbers = new int[n];
        IntStream.range(0, n).forEach(i -> numbers[i] = scanner.nextInt());
        quicksort(numbers, 0, numbers.length - 1);
    }
}
