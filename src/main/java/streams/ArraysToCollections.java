package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public class ArraysToCollections {
    public static void main(String[] args) {
        String[] strings = new String[]{"Hello", "World"};
        List<String> stringList = stringArrayToList(strings);
        stringList.forEach(System.out::println);
        System.out.println();

        System.out.println("int array to List<Integer>");
        int[] integers = new int[]{1, 2, 3, 4, 2, 3, 2, 1};
        List<Integer> integerList = intArrayToIntegerList(integers);
        integerList.forEach(System.out::println);
        System.out.println();

        System.out.println("Sorting List<Integer>");
        Collections.sort(integerList);
        integerList.forEach(System.out::println);
        System.out.println();

        System.out.println("Reverse Sorting List<Integer>");
        List<Integer> reverseOrderedIntegers = new ArrayList<>(integerList);
        Collections.reverse(reverseOrderedIntegers);
        reverseOrderedIntegers.forEach(System.out::println);
        System.out.println();
        Integer endValue = reverseOrderedIntegers.get(reverseOrderedIntegers.size() - 1);
        endValue = 1;

        System.out.println("Checking original List after reverse");
        integerList.forEach(System.out::println);
        System.out.println();

        System.out.println("boolean array to List<Boolean>");
        boolean[] bools = new boolean[]{true, false, true, true};
        List<Boolean> booleanList = booleanArrayToBooleanList(bools);
        booleanList.forEach(System.out::println);
        System.out.println();
    }

    public static List<String> stringArrayToList(String[] strings) {
        List<String> result = Stream.of(strings).collect(Collectors.toList());
        return result;
    }

    public static List<Integer> intArrayToIntegerList(int[] integers) {
        List<Integer> result = Arrays.stream(integers)
                .boxed()
                .collect(Collectors.toList());
        return result;
    }

    public static List<Boolean> booleanArrayToBooleanList(boolean[] booleans) {
        List<Boolean> result = new ArrayList<>();
        for (boolean b : booleans) {
            result.add(b);
        }

        List<Boolean> result2 = IntStream.iterate(0, i -> i+1)
                .limit(booleans.length)
                .mapToObj(i -> booleans[i])
                .collect(Collectors.toList());

        return result2;
    }
}
