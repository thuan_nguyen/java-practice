package algorithms.binaryindextree;

import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Author: Tony Nguyen on 5/06/17.
 */
public class XorBITTest {
    final static int[] DEFAULT_INPUT = {1, 0, 0, 1, 0, 1, 1, 0, 1, 1};
    private static BinaryIndexTree<Integer> BIT;

    @Before
    public void before() {
        Integer[] input = IntStream.of(DEFAULT_INPUT).boxed().toArray(Integer[]::new);
        BIT = new XorBIT(input);
    }

    @Test
    public void fold_canFoldFromStart() throws Exception {
        final int EXPECTED = 1;
        final int INDEX = 0;
        int result = BIT.fold(INDEX);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void fold_canFoldToMiddle() throws Exception {
        final int EXPECTED = 1;
        final int INDEX = 5;
        int result = BIT.fold(INDEX);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void fold_canFoldToEnd() throws Exception {
        final int EXPECTED = 0;
        final int INDEX = DEFAULT_INPUT.length - 1;
        int result = BIT.fold(INDEX);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldFromStart() throws Exception {
        final int EXPECTED = 1;
        final int LOWER = 0, UPPER = 2;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldFromStart_1awayFromStart() throws Exception {
        final int EXPECTED = 0;
        final int LOWER = 1, UPPER = 2;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldFromStart_1more() throws Exception {
        final int EXPECTED = 0;
        final int LOWER = 0, UPPER = 3;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldToEnd() throws Exception {
        final int EXPECTED = 1;
        final int UPPER = DEFAULT_INPUT.length - 1, LOWER = UPPER - 3;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldToEnd_1awayFromEnd() throws Exception {
        final int EXPECTED = 0;
        final int UPPER = DEFAULT_INPUT.length - 2, LOWER = UPPER - 2;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldToEnd_oneMore() throws Exception {
        final int EXPECTED = 0;
        final int UPPER = DEFAULT_INPUT.length - 1, LOWER = UPPER - 4;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldMiddle() throws Exception {
        final int EXPECTED = 1;
        final int LOWER = 3, UPPER = 6;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldMiddle_oneLessLeft() throws Exception {
        final int EXPECTED = 0;
        final int LOWER = 4, UPPER = 6;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_canFoldMiddle_oneLessRight() throws Exception {
        final int EXPECTED = 0;
        final int LOWER = 3, UPPER = 5;
        int result = BIT.foldRange(LOWER, UPPER);
        assertThat(result).isEqualTo(EXPECTED);
    }
}