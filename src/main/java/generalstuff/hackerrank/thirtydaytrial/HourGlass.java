package generalstuff.hackerrank.thirtydaytrial;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 30/05/17.
 */
public class HourGlass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                arr[i][j] = in.nextInt();
            }
        }
        int max = maxSumHourglass(arr);
        System.out.println(max);
    }

    public static int maxSumHourglass(int[][] array) {
        int nRows = array.length;
        int nColumns = array[0].length;

        int rLimit = nRows - 2;
        int cLimit = nColumns - 2;

        int max = sumHourglass(array, 0, 0);
        for(int i = 0; i < rLimit; i++) {
            for(int j = 0; j < cLimit; j++) {
                System.out.println("i: " + i + ", j: " + j);
                int sum = sumHourglass(array, i, j);
                System.out.println("sum: " + sum + "\n");
                if(sum > max) max = sum;
            }
        }
        return max;
    }

    public static int sumHourglass(int[][] array, int row, int column) {
        int topRow = row;
        int bottomRow = row + 2;

        int colLimit = column + 2;

        int sum = 0;
        for(int i = column; i <= colLimit; i++) {
            sum += (array[topRow][i] + array[bottomRow][i]);
        }

        sum += array[row + 1][column + 1];
        return sum;
    }
}
