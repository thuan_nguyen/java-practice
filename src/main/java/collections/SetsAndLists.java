package collections;

import java.util.*;

/**
 * Created by Tony Nguyen on 19/05/17.
 */
public class SetsAndLists {
    public static void main(String[] args) {
        System.out.println("Basic HashSet");
        Set<String> basicSet = basicHashSet();
        System.out.println(basicSet.add("hello"));
        basicSet.forEach(System.out::println);
        System.out.println("\n");

        System.out.println("Basic ArrayList");
        basicArrayList().forEach(System.out::println);
        System.out.println("\n");

        System.out.println("Construct ArrayList with String array");
        constructArrayListWithStringArray().forEach(System.out::println);
        System.out.println("\n");
    }

    public static Set<String> basicHashSet() {
        Set<String> strings = new HashSet<>();
        strings.add("hello");
        strings.add("world");
        strings.add("!");
        return strings;
    }

    public static List<String> basicArrayList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("hello");
        stringList.add("world");
        stringList.add("!");
        return stringList;
    }

    public static List<String> constructArrayListWithStringArray() {
        String[] strings = new String[]{"hello", "world", "!"};

        List<String> stringList = new ArrayList<>(Arrays.asList(strings));
        return stringList;
    }
}
