package algorithms.fibonacci;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public abstract class FibonacciTest {
    private Fibonacci FIBONACCI;

    /**
     * Initialize FibonacciTest by setting the Fibonacci member with given
     * implementation. Use <code>setFibonacci</code> to configure the member.
     */
    @Before
    public abstract void initializeFibonacci();

    protected void setFibonacci(Fibonacci fibonacci) {
        this.FIBONACCI = fibonacci;
    }

    @Test
    public void testZero_returnsList_With0() {
        final int INPUT = 0;
        List<Integer> fibonacciList = FIBONACCI.fibonacciList(INPUT);
        assertTrue(fibonacciList.size() == 1);
        assertTrue(fibonacciList.get(0) == 0);
    }

    @Test
    public void testOne_returnsList_With0And1() {
        final int INPUT = 1;
        List<Integer> fibonacciList = FIBONACCI.fibonacciList(INPUT);
        assertTrue(fibonacciList.size() == 2);
        assertTrue(fibonacciList.get(0) == 0);
        assertTrue(fibonacciList.get(1) == 1);
    }

    @Test
    public void testTen_returnsList_withCorrectValues() {
        final int INPUT = 10;
        final int[] EXPECTED_ARRAY = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55};

        List<Integer> expected = IntStream.of(EXPECTED_ARRAY)
                .boxed()
                .collect(toList());

        List<Integer> fibonacciList = FIBONACCI.fibonacciList(INPUT);

        assertTrue("fibonacciList does not contain all of expected",
                fibonacciList.containsAll(expected));

        assertTrue("size not equal",
                fibonacciList.size() == expected.size());
    }
}
