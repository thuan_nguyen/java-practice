package generalstuff.hackerrank.warmup;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class TimeConversionTest {
    @Test
    public void convertTo24Hour_beforeNoon() throws Exception {
        String INPUT = "11:59:59AM";
        String EXPECTED = "11:59:59";

        String output = TimeConversion.convertTo24Hour(INPUT);
        assertThat(output).isEqualTo(EXPECTED);
    }

    @Test
    public void convertTo24Hour_afterNoon() throws Exception {
        String INPUT = "12:00:01PM";
        String EXPECTED = "12:00:01";

        String output = TimeConversion.convertTo24Hour(INPUT);
        assertThat(output).isEqualTo(EXPECTED);
    }

    @Test
    public void convertTo24Hour_at_1Pm() throws Exception {
        String INPUT = "01:00:00PM";
        String EXPECTED = "13:00:00";

        String output = TimeConversion.convertTo24Hour(INPUT);
        assertThat(output).isEqualTo(EXPECTED);
    }

    @Test
    public void convertTo24Hour_beforeMidnight() throws Exception {
        String INPUT = "11:59:59PM";
        String EXPECTED = "23:59:59";

        String output = TimeConversion.convertTo24Hour(INPUT);
        assertThat(output).isEqualTo(EXPECTED);
    }
}