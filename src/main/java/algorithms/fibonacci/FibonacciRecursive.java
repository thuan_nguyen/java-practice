package algorithms.fibonacci;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public final class FibonacciRecursive implements Fibonacci {
    @Override
    public int fibonacci(int n) {
        if(n <= 0) return 0;
        if (n == 1) return 1;

        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    @Override
    public List<Integer> fibonacciList(int n) {
        return IntStream.range(0, n + 1)
                .map(FibonacciRecursive::calculateFibonacci)
                .boxed()
                .collect(Collectors.toList());
    }

    private static int calculateFibonacci(int n) {
        return new FibonacciRecursive().fibonacci(n);
    }
}
