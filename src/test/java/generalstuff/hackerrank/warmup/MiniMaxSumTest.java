package generalstuff.hackerrank.warmup;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class MiniMaxSumTest {
    @Test
    public void miniMaxSum() throws Exception {
        long[] INPUT = {1, 2, 3, 4, 5};
        long[] EXPECTED = {10, 14};

        long[] result = MiniMaxSum.miniMaxSum(INPUT);

        assertThat(result).containsExactly(EXPECTED);
    }

}