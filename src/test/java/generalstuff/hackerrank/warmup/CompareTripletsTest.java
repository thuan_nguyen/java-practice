package generalstuff.hackerrank.warmup;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class CompareTripletsTest {
    @Test
    public void testSolve() {
        final int[] EXPECTED = {1, 1};
        final int a1 = 5, a2 = 6, a3 = 7;
        final int b1 = 3, b2 = 6, b3 = 10;

        int[] result = CompareTriplets.solve(a1, a2, a3, b1, b2, b3);

        assertThat(result).containsExactly(EXPECTED);
    }
}