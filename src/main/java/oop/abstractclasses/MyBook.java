package oop.abstractclasses;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class MyBook extends Book {
    private int price;

    public MyBook(String title, String author, int price) {
        super(title, author);
        this.price = price;
    }

    @Override
    void display() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Title: " + this.title
                + "\n" + "Author: " + this.author
                + "\n" + "Price: " + this.price;
    }
}
