package generalstuff.hackerrank.warmup;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class BigSumTest {
    @Test
    public void sum() {
        long[] INPUT = {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
        long EXPECTED = 5000000015L;
        long sum = BigSum.sum(INPUT);

        assertThat(sum).isEqualTo(sum);
    }
}