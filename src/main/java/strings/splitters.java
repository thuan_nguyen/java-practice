package strings;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class splitters {
    public static void main(String[] args) {
        String string = "This is , just|a, test";
        String[] strings = string.replaceAll("[, |]", ",")
                .split(",+");
        Stream.of(strings).forEach(System.out::println);

        Scanner scanner = new Scanner(System.in);
        scanner.next();
        clear();

        String hello = "hello";
        System.out.println(hello.matches("^hello$"));

        String helloAgain = "hello again";
        System.out.println(helloAgain.matches(".*hello.*"));
    }

    public String[] splitString(String string, String pattern) {
        return string.split(pattern);
    }

    public static void clear() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
