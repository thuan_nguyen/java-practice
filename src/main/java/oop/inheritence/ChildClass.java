package oop.inheritence;

/**
 * Created by Tony Nguyen on 3/06/17.
 */
public class ChildClass extends ParentClass {
    private String name = "child";

    public ChildClass() {
        super();
    }

    public ChildClass(String name) {
        super("super: " + name);
        this.name = name;
    }

//    public String getName() {
//        return name;
//    }

    public void printNameNoOverride() {
        System.out.println("ChildClass: printNameNoOverride");
        System.out.println(this.name);
    }

    @Override
    public void printNameWithOverride() {
        System.out.println("ChildClass: printNameWithOverride");
        System.out.println(this.name);
    }
}
