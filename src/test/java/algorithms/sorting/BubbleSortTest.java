package algorithms.sorting;

import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class BubbleSortTest extends SortTest {
    @Override
    public void initializeSorter() {
        setup(new BubbleSort());
    }

    @Test
    public void isSorted_returnsTrue_forSortedValues() {
        int[] ints = {1, 2, 3, 4, 5};
        List<Integer> integerList = IntStream.of(ints).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compare;

        boolean result = BubbleSort.isSorted(integerList, comparator);
        assertThat(result).isTrue();
    }

    @Test
    public void isReversed_returnsTrue_forReversedValues() {
        int[] INPUT = {1, 2, 3, 4, 5};
        int[] EXPECTED = {5, 4, 3, 2, 1};

        List<Integer> integerList = IntStream.of(INPUT).boxed()
                .collect(Collectors.toList());

        Comparator<Integer> comparator = Integer::compare;

        boolean isSorted = BubbleSort.isSorted(integerList, comparator);
        assertThat(isSorted).isTrue();

        getSorter().reverse(integerList, comparator);
        boolean isReversed = BubbleSort.isReversed(integerList, comparator);

        assertThat(isReversed).isTrue();
    }
}