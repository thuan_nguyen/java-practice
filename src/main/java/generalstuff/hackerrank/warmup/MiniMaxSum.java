package generalstuff.hackerrank.warmup;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class MiniMaxSum {
    private final static int MIN_INDEX = 0;
    private final static int MAX_INDEX = 1;
    private final static int SUM_INDEX = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final int n = 5;

        long[] numbers = new long[n];
        IntStream.range(0, n).forEach((i) -> numbers[i] = scanner.nextLong());

        long[] minimumMaxSum = miniMaxSum(numbers);
        System.out.println(minimumMaxSum[0] + " " + minimumMaxSum[1]);
    }

    public static long[] miniMaxSum(long[] numbers) {
        long[] minMax = minMaxSum(numbers);
        long minSum = minMax[SUM_INDEX] - minMax[MAX_INDEX];
        long maxSum = minMax[SUM_INDEX] - minMax[MIN_INDEX];

        return new long[]{minSum, maxSum};
    }

    public static long[] minMaxSum(long[] numbers) {
        long[] minMaxSum = new long[3];
        long min = numbers[0];
        long max = min;
        long sum = 0L;

        for(long number : numbers) {
            if (number < min) min = number;
            if (number > max) max = number;
            sum += number;
        }

        minMaxSum[MIN_INDEX] = min;
        minMaxSum[MAX_INDEX] = max;
        minMaxSum[SUM_INDEX] = sum;
        return minMaxSum;
    }
}
