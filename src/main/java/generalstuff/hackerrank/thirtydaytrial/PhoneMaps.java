package generalstuff.hackerrank.thirtydaytrial;


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

class Solution{
    public static void main(String []args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        PhoneBook phoneBook = new PhoneBook();
        for(int i = 0; i < n; i++){
            String name = in.next();
            String phone = in.next();
            phoneBook.addContact(name, phone);
            // Write code here
        }

        String[] names = new String[n];
        while(in.hasNext()){
            String s = in.next();
            phoneBook.printContactWithName(s);
        }
        in.close();
    }

    public static class PhoneBook {
        private Map<String, String> contacts;

        public PhoneBook() {
            contacts = new HashMap<>();
        }

        public String addContact(String name, String number) {
            return contacts.putIfAbsent(name, number);
        }

        public Stream getContacts(String[] names) {
            return Stream.of(names)
                    .filter(name -> contacts.containsKey(name));
        }

        public void printContactsWithNames(String[] names) {
            StringBuilder builder = new StringBuilder();

            Stream matchingNames = getContacts(names);
            matchingNames.forEach(name -> {
                builder.append(name);
                builder.append(" ");
                builder.append(contacts.get(name));
                builder.append("\n");
            });

            System.out.println(builder.toString());
        }

        public void printContactWithName(String name) {
            StringBuilder builder = new StringBuilder();
            if(contacts.containsKey(name)) {
                builder.append(name);
                builder.append("=");
                builder.append(contacts.get(name));
            } else {
                builder.append("Not found");
            }

            System.out.println(builder.toString());
        }
    }
}
