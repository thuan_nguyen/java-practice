package generalstuff.hackerrank.implementation;

public class Kangaroo implements Comparable<Kangaroo> {
    private int position;
    private int leapDistance;
    private int id;
    private static int idCounter = 0;

    public Kangaroo(int position, int leapDistance) {
        this.position = position;
        this.leapDistance = leapDistance;
        this.id = idCounter++;
    }

    public void jump() {
        position += leapDistance;
    }

    public int getLeapDistance() {
        return leapDistance;
    }

    public int getPosition() {
        return position;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Kangaroo)) return false;

        Kangaroo kangaroo = (Kangaroo) o;

        if (position != kangaroo.position) return false;
        if (leapDistance != kangaroo.leapDistance) return false;
        return id == kangaroo.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Kangaroo kangaroo) {
        return Integer.compare(this.getPosition(), kangaroo.getPosition());
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"position\":" + position +
                ", \"leapDistance\":" + leapDistance +
                '}';
    }
}
