package algorithms.fibonacci;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class FibonacciRecursiveTest extends FibonacciTest {
    @Override
    public void initializeFibonacci() {
        setFibonacci(new FibonacciRecursive());
    }
}