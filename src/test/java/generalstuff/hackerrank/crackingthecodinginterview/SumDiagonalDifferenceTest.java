package generalstuff.hackerrank.crackingthecodinginterview;

import generalstuff.hackerrank.warmup.SumDiagonalDifference;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class SumDiagonalDifferenceTest {
    @Test
    public void differenceOfSumOfDiagonals() throws Exception {
        int[][] INPUT = new int[][]{
                {11, 2, 4},
                {4, 5, 6},
                {10, 8, -12}
        };

        int EXPECTED = 15;

        int result = SumDiagonalDifference.differenceOfSumOfDiagonals(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }

}