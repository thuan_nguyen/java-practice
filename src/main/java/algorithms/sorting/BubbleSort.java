package algorithms.sorting;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class BubbleSort implements Sort {
    public static boolean isSorted(List values, Comparator comparator) {
        for(int i = 1; i < values.size(); i++) {
            if(comparator.compare(values.get(i-1), values.get(i)) > 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isReversed(List values, Comparator comparator) {
        return isSorted(values, comparator.reversed());
    }

    @Override
    public void sort(List values, Comparator comparator) {
        if(isSorted(values, comparator)) return;

        for(int i = values.size(); i >= 0; i--) {
            for(int j = 1; j < i; j++) {
                swap(values, comparator, j);
            }
        }
    }

    @Override
    public void reverse(List values, Comparator comparator) {
        sort(values, comparator.reversed());
    }

    public void swap(List values, Comparator comparator, int index) {
        Object j1 = values.get(index - 1);
        Object j2 = values.get(index);
        int comparison = comparator.compare(j1, j2);
        if(comparison > 0) {
            values.set(index - 1, j2);
            values.set(index, j1);
        }
    }
}
