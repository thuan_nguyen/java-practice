package generalstuff.hackerrank.warmup;

import java.util.Scanner;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class PlusMinus {
    public final static int POSITIVES = 0;
    public final static int NEGATIVES = 1;
    public final static int ZEROS = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] numbers = new int[n];
        IntStream.range(0, n)
                .forEach((i) -> numbers[i] = scanner.nextInt());

        double[] fractions = signFractions(numbers);
        printFractions(fractions);
    }

    public static double[] signFractions(int[] numbers) {
        double[] fractions = new double[]{0.0, 0.0, 0.0};
        int sumZeros = 0, sumPositives = 0, sumNegatives = 0;

        for(int i = 0; i < numbers.length; i++) {
            if(numbers[i] > 0) {
                ++sumPositives;
            } else if(numbers[i] < 0) {
                ++sumNegatives;
            } else {
                ++sumZeros;
            }
        }

        fractions[POSITIVES] = (double)sumPositives/(double)numbers.length;
        fractions[NEGATIVES] = (double)sumNegatives/(double)numbers.length;
        fractions[ZEROS] = (double)sumZeros/(double)numbers.length;

        return fractions;
    }

    public static void printFractions(double[] fractions) {
        DoubleStream.of(fractions).forEach(System.out::println);
    }
}
