package algorithms.fibonacci;

import java.util.List;

/**
 * Created by Tony Nguyen on 20/05/17.
 */
public interface Fibonacci {
    /**
     * @param n
     * @return returns the fibonacci value for Fn
     */
    int fibonacci(int n);

    /**
     * @param n
     * @return returns a list of fibonacci sequences from 0 to n
     */
    List<Integer> fibonacciList(int n);
}
