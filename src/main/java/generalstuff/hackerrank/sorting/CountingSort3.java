package generalstuff.hackerrank.sorting;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class CountingSort3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] numbers = new int[100];
        String[] strings = new String[n];
        IntStream.range(0, n).forEach(i -> {
            numbers[scanner.nextInt()]++;
            strings[i] = scanner.next();
        });

        int sum = 0;
        for(int number : numbers) {
            sum += number;
            System.out.print(sum + " ");
        }
        System.out.println();
    }
}
