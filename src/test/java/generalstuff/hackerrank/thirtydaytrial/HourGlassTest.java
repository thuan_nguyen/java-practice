package generalstuff.hackerrank.thirtydaytrial;

import org.junit.Test;

import static generalstuff.hackerrank.thirtydaytrial.HourGlass.maxSumHourglass;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 30/05/17.
 */
public class HourGlassTest {
    @Test
    public void sumHourglass_passesDefaultTest() throws Exception {
        int[][] INPUT = {{1, 1, 1, 0, 0, 0},
                         {0, 1, 0, 0, 0, 0},
                         {1, 1, 1, 0, 0, 0},
                         {0, 0, 2, 4, 4, 0},
                         {0, 0, 0, 2, 0, 0},
                         {0, 0, 1, 2, 4, 0}};

        int EXPECTED = 19;

        int result = maxSumHourglass(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void sumHourglass_canDoColumns() throws Exception {
        int[][] INPUT = {{1, 1, 1, 2, 4, 4},
                         {0, 1, 0, 0, 2, 0},
                         {1, 1, 1, 1, 2, 4}};

        int EXPECTED = 19;

        int result = maxSumHourglass(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void sumHourglass_canDoRows() throws Exception {
        int[][] INPUT = {{1, 1, 1, 0, 0, 0},
                         {0, 1, 0, 0, 0, 0},
                         {1, 1, 1, 0, 0, 0},
                         {0, 0, 0, 2, 4, 4},
                         {0, 0, 0, 0, 2, 0},
                         {0, 0, 0, 1, 2, 4}};


        int EXPECTED = 19;

        int result = maxSumHourglass(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }


    @Test
    public void sumHourglass_canDoCorners() throws Exception {
        int[][] INPUT = {{1, 1, 1},
                {0, 1, 0},
                {1, 1, 1},
                {2, 4, 4},
                {0, 2, 0},
                {1, 2, 4}};

        int EXPECTED = 19;

        int result = maxSumHourglass(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void sumHourglass_canDoFilledArrays() throws Exception {
        int[][] INPUT = {{1, 2, 3, 0, 0, 0},
                         {4, 5, 6, 0, 0, 0},
                         {7, 8, 9, 0, 0, 0},
                         {0, 0, 0, -9, -9, -9},
                         {0, 0, 0, -9, -9, -9},
                         {0, 0, 0, -9, -9, -9}};


        int EXPECTED = 35;

        int result = maxSumHourglass(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }
}