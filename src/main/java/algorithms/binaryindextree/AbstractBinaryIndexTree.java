package algorithms.binaryindextree;

import lambdas.Operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static utilities.MyUtilities.arrayToString;
import static utilities.MyUtilities.listToString;

/**
 * Author: Tony Nguyen on 4/06/17.
 */
public abstract class AbstractBinaryIndexTree<T> implements BinaryIndexTree<T> {
    private final Operator<T> operator;

    private final T[] original;
    private final T[] bit;

    protected AbstractBinaryIndexTree(T[] array, Operator<T> operator) {
        this.operator = operator;
        original = Arrays.copyOf(array, array.length);
        this.bit = initialize(original);
        setupBit();
    }

    protected abstract T[] initialize(T[] array);

    private void setupBit() {
        IntStream.range(0, bit.length).forEach(i -> update(i, original[i]));
    }

    public void update(int index, T value){
        index += 1;
        while(index <= bit.length) {
            bit[index - 1] = operator.operate(bit[index - 1], value);
            index += index & -index;
        }
    }

    public T fold(int end){
        if(end >= bit.length || end < 0) throw new IndexOutOfBoundsException("OUT OF RANGE");
        int index = end + 1;

        T folded = bit[index - 1];
        index -= index & -index;

        while(index >= 1) {
            folded = operator.operate(folded, bit[index - 1]);
            index -= index & -index;
        }

        return folded;
    }

    public int length() {
        return bit.length;
    }

    public T getPointInBit(int index) {
        return bit[index];
    }

    public T getPointInOriginal(int index) {
        return original[index];
    }

    public abstract T foldRange(int start, int end);

    public String originalToString() {
//        if(original == null) return bit.toString();
        return arrayToString(original);
    }

    public String bitToString() {
//        if(bit == null) return bit.toString();
        return arrayToString(bit);
    }

    @Override
    public String cumulativeString() {
        StringBuilder builder = new StringBuilder();
        List<T> cumulative = new ArrayList<>();
        cumulative.add(original[0]);
        IntStream.range(1, original.length)
                .forEach(i -> cumulative.add(operator.operate(cumulative.get(i - 1), original[i])));

        return listToString(cumulative);
    }

    @Override
    public String toString() {
        return "original:\n" + originalToString()
                + "\n\nbit:\n" + bitToString()
                + "\n\ncumulative:\n" + cumulativeString();
    }
}
