package flowandcontrol;

import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class BasicLoops {
    public static void main(String[] args) {
        System.out.println("WHILE LOOP");
        int i = 0;
        while (i < 10) {
            System.out.println("i = " + i);
            ++i;
        }
        System.out.println();

        System.out.println("WHILE LOOP i++");
        i = 0;
        while (i++ < 10) {
            System.out.println("i = " + i);
        }
        System.out.println();

        System.out.println("DO WHILE LOOP");
        i = 0;
        do {
            System.out.println("i = " + i);
            i++;
        } while (i < 10);
        System.out.println();

        System.out.println("DO WHILE LOOP i++");
        i = 0;
        do {
            System.out.println("i = " + i);
        } while (i++ < 10);
        System.out.println();


        System.out.println("FOR LOOP");
        for(i = 0; i < 10; i++) {
            System.out.println("i = " + i);
        }
        System.out.println();

        int limit = 10;
        intStream(limit);

        System.out.println("Recursion Loop");
        recursionLoop(0, 10);
    }

    public static void intStream(int limit) {
        System.out.println("IntStream iterate");
        IntStream.iterate(0, (j) -> j + 1).limit(limit)
            .forEach((j) -> System.out.println("j = " + j));
        System.out.println();
    }


    public static void recursionLoop(int index, int limit) {
        if (index < limit) {
            System.out.println("index = " + index);
            recursionLoop(++index, limit);
//            recursionLoop(index++, limit); // will cause infinite loop because iteration is not done until after method call
        }
    }
}
