package primitives;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class Primitives {
    public static void main(String[] args) {
        byte myByte = 5;
        char myChar = 5;
        short myShort = 5;
        int myInt = 5;
        long myLong = 5L;

        float myFloat = 5f;
        double myDouble = 5d;

        boolean myBoolean = true;

        System.out.println(myByte);
        System.out.println(myByte);
        System.out.println(myChar);
        System.out.println(myShort);
        System.out.println(myInt);
        System.out.println(myLong);

        System.out.println(myFloat);
        System.out.println(myDouble);

        System.out.println(myBoolean);
    }
}
