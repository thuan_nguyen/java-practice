package generalstuff.hackerrank.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class quicksort {
    public static List<Integer> quicksort(List<Integer> array) {
        List<List<Integer>> partition = partition(array);
        List<Integer> left = partition.get(0);
        List<Integer> equal = partition.get(1);
        List<Integer> right = partition.get(2);

        if (left.size() > 1) {
            left = quicksort(partition.get(0));
        }

        if (right.size() > 1) {
            right = quicksort(partition.get(2));
        }

        List<Integer> merged = new ArrayList(left);
        merged.addAll(equal);
        merged.addAll(right);
        printArray(merged);
        return merged;
    }

    public static List<List<Integer>> partition(List<Integer> list) {
        Integer pivot = list.get(0);

        List<List<Integer>> partitions = new ArrayList<>();
        List<Integer> leftPartition = new ArrayList<>();
        List<Integer> rightPartition = new ArrayList<>();
        List<Integer> equalPartition = new ArrayList<>();

        for (Integer element : list) {
            if (element < pivot) leftPartition.add(element);
            else if (element > pivot) rightPartition.add(element);
            else equalPartition.add(element);
        }

        partitions.add(leftPartition);
        partitions.add(equalPartition);
        partitions.add(rightPartition);

        return partitions;
    }

    public static void printArray(List<Integer> array) {
        IntStream.range(0, array.size() - 1).forEach(
                i -> System.out.print(array.get(i) + " ")
        );
        System.out.println(array.get(array.size() - 1));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] unsorted = new int[n];
        IntStream.range(0, n).forEach(i -> unsorted[i] = scanner.nextInt());

        List<Integer> array = IntStream.of(unsorted).boxed().collect(Collectors.toList());

        List<Integer> sorted = quicksort(array);
    }
}


//    public static void swap(int[] array, int p1, int p2) {
//        int temp = array[p1];
//        array[p1] = array[p2];
//        array[p2] = temp;
//    }
//
//    public static int[] merge(int[] left, int[] right) {
//        int[] mergedArray = new int[left.length + right.length];
//        IntStream.range(0, left.length)
//                .forEach(i -> mergedArray[i] = left[i]);
//
//        IntStream.range(left.length, mergedArray.length)
//                .forEach(i -> mergedArray[i] = right[i - left.length]);
//
//        return mergedArray;
//    }
