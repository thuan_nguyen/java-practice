package primitives;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class BasicNumbers {
    final static double KG_PER_POUND = 0.45359237;

    public static void main(String[] args) {
        int myInt = 5;
        float myFloat = 5f;
        double myDouble = 5.0d;

        System.out.println(poundToKg(200));
    }

    static double poundToKg(double pounds) {
        return pounds * KG_PER_POUND;
    }
}
