package generalstuff.hackerrank.strings;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class PangramsTest {
    private final String PANGRAM = "We promptly judged antique ivory buckles for the next prize";
    private final String NOTPANGRAM = "We promptly judged antique ivory buckles for the prize";
    private final String PANGRAM_MESSAGE = "pangram";
    private final String NOTPANGRAM_MESSAGE = "not pangram";

    @Test
    public void message_returns_pangram() throws Exception {
        final String SENTENCE = PANGRAM;
        String message = Pangrams.message(Pangrams.isPangram(SENTENCE));
        assertEquals(PANGRAM_MESSAGE, message);
    }

    @Test
    public void isPangram_returnsTrue() throws Exception {
        final String SENTENCE = PANGRAM;
        boolean result = Pangrams.isPangram(SENTENCE);

        assertTrue(result);
    }

    @Test
    public void message_returns_notpangram() throws Exception {
        final String SENTENCE = NOTPANGRAM;
        String message = Pangrams.message(Pangrams.isPangram(SENTENCE));
        assertEquals(NOTPANGRAM_MESSAGE, message);
    }

    @Test
    public void isPangram_returnsFalse() throws Exception {
        final String SENTENCE = NOTPANGRAM;
        boolean result = Pangrams.isPangram(SENTENCE);

        assertFalse(result);
    }

}