package generalstuff.hackerrank.sorting;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class IntroTutorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberToSearch = scanner.nextInt();
        int numberOfElements = scanner.nextInt();
        int index = -1;

        int[] array = new int[numberOfElements];
        boolean notFound = true;

        for(int i = 0; i < numberOfElements; i++) {
            array[i] = scanner.nextInt();
            if(notFound) {
                if (array[i] == numberToSearch) {
                    index = i;
                    notFound = true;
                }
            }
        }
        System.out.println(index);
    }

    public static int binarySearch(int[] array, int numberToSearch) {
        int i = 0;
        int j = array.length;

        while(i < j) {
            int p = i + (j - i)/2;
            if(numberToSearch < array[p]) j = p - 1;
            if(numberToSearch > array[p]) i = p + 1;

            if(numberToSearch == array[p]) return p;
        }

        return -1;
    }
}
