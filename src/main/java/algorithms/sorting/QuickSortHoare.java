package algorithms.sorting;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class QuickSortHoare implements Sort {
    @Override
    public void sort(List values, Comparator comparator) {
        quicksort(values, 0, values.size() - 1, comparator);
    }

    @Override
    public void reverse(List values, Comparator comparator) {
        sort(values, comparator.reversed());
    }

    public <T> void quicksort(List<T> list, int low, int high, Comparator<T> comparator) {
        if (low < high) {
            int pivot = partition(list, low, high, comparator);
            quicksort(list, low, pivot - 1, comparator);
            quicksort(list, pivot, high, comparator);
        }
    }

    private void swap(List list, int p1, int p2) {
        Object val1 = list.get(p1);
        Object val2 = list.get(p2);

        list.set(p1, val2);
        list.set(p2, val1);
    }

    public int partition(List list, int low, int high, Comparator comparator) {
        int midIndex = (low + high)/2;
        Object midItem = list.get(midIndex);
        while(low <= high) {
            while (comparator.compare(list.get(low), midItem) < 0) {
                ++low;
            }

            while (comparator.compare(list.get(high), midItem) > 0) {
                --high;
            }

            if(low <= high) {
                swap(list, low, high);
                ++low;
                --high;
            }
        }

        return low;
    }

    public void nothing() {
        String[] strings = {"6", "31415926535897932384626433832795", "1", "3", "10", "3", "5"};
        List<BigInteger> bigInts = Stream.of(strings).map(BigInteger::new).collect(Collectors.toList());

//        ).mapToInt(Integer::new).collect(Collectors.toList());
    }
}
