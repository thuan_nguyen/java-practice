package generalstuff.hackerrank.strings;

import org.junit.Test;

import static generalstuff.hackerrank.strings.FunnyString.stringIsFunny;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 29/05/17.
 */
public class FunnyStringTest {
    @Test
    public void stringIsFunny_passesDefaultTest() {
        String INPUT = "acxz";
        String EXPECTED = "Funny";

        String result = stringIsFunny(INPUT);

        assertThat(result).isEqualTo(EXPECTED);
    }
}