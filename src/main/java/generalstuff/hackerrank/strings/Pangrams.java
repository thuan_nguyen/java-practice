package generalstuff.hackerrank.strings;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class Pangrams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine();
        System.out.println(message(isPangram(sentence)));
    }

    public static String message(boolean isPangram) {
        return isPangram ? "pangram" : "not pangram";
    }

    public static boolean isPangram(String text) {
        Set<Character> characters = new HashSet<>();
        String upperCaseText = text.toUpperCase();

        for(int i = 0; i < text.length(); i++) {
            if(upperCaseText.charAt(i) >= 'A' && upperCaseText.charAt(i) <= 'Z') {
                characters.add(upperCaseText.charAt(i));
            }
        }
        return characters.size() == 26;
    }
}
