package generalstuff;

import org.junit.Test;

import static generalstuff.CsgProblem1.checkAllMatch;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class CsgProblem1Test {
    @Test
    public void test_OneToFive() {
        String[] input1 = {"1", "2", "3", "4", "5"};
        String[] input2 = {"4", "2", "3", "1", "5"};

        boolean result = checkAllMatch(input1, input2);

        assertTrue(result);
    }

//    @Test
//    public void test_OneToFive_withString() {
//        String input1 = "1 2 3 4 5";
//        String input2 = "4 2 3 1 5 5";
//
//        boolean result = checkAllMatch(input1, input2);
//
//        assertTrue(result);
//    }
}