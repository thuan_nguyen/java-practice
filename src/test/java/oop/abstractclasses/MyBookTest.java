package oop.abstractclasses;

import org.junit.Test;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class MyBookTest {
    @Test
    public void display_canDisplayWithoutError() throws Exception {
        final String TITLE = "The Alchemist";
        final String AUTHOR = "Paulo Coelho";
        final int PRICE = 248;

        MyBook result = new MyBook(TITLE, AUTHOR, PRICE);
        System.out.println(result);
    }

}