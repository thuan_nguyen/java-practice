package generalstuff.hackerrank.sorting;

import org.junit.Test;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class Quicksort3Test {
    @Test
    public void testDefault() {
        int[] numbers = {1, 3, 9, 8, 2, 7, 5};
//        Quicksort3.printIntArray(numbers);
        Quicksort3.quicksort(numbers, 0, numbers.length - 1);
    }

    @Test
    public void testLowerBoundary() {
        int[] numbers = {3, 3, 9, 8, 2, 7, 1};
        Quicksort3.printArray(numbers);
        Quicksort3.quicksort(numbers, 0, numbers.length - 1);
    }

    @Test
    public void testBullshit() {
//        1 2 6 7 3 5 4 9 8
//        1 2 6 7 3 5 4 8 9
//        1 2 3 4 6 5 7 8 9
//        1 2 3 4 6 5 7 8 9
//        1 2 3 4 5 6 7 8 9
        int[] numbers = {9, 8, 6, 7, 3, 5, 4, 1, 2};
        Quicksort3.printArray(numbers);
        Quicksort3.quicksort(numbers, 0, numbers.length - 1);
    }
}