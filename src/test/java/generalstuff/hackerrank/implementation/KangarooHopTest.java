package generalstuff.hackerrank.implementation;

import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class KangarooHopTest {
    @Test
    public void testKangaroosWillMeet() throws Exception {
//        final int[] INPUT = {1408, 6689, 6730, 4028};
        final int[] INPUT = parseStringInput("1408 6689 6730 4028");
        final String EXPECTED = "YES";

        KangarooHop.RacingKangaroo[] kangaroos = createKangaroos(INPUT);

        KangarooHop.simulate(kangaroos[0], kangaroos[1]);
        String result = KangarooHop.kangaroosWillMeet(kangaroos[0], kangaroos[1]);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void testKangaroosWillMeet_case21() throws Exception {
        final int[] INPUT = parseStringInput("4602 8519 7585 8362");
        final String EXPECTED = "YES";

        KangarooHop.RacingKangaroo[] kangaroos = createKangaroos(INPUT);

        KangarooHop.simulate(kangaroos[0], kangaroos[1]);
        String result = KangarooHop.kangaroosWillMeet(kangaroos[0], kangaroos[1]);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void testKangaroosWillMeet_case25() throws Exception {
        final int[] INPUT = parseStringInput("1113 612 1331 610");
        final String EXPECTED = "YES";

        KangarooHop.RacingKangaroo[] kangaroos = createKangaroos(INPUT);

        KangarooHop.simulate(kangaroos[0], kangaroos[1]);
        String result = KangarooHop.kangaroosWillMeet(kangaroos[0], kangaroos[1]);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void testKangaroosWillMeet_case27() throws Exception {
        final int[] INPUT = parseStringInput("1928 4306 5763 4301");
        final String EXPECTED = "YES";

        KangarooHop.RacingKangaroo[] kangaroos = createKangaroos(INPUT);

        KangarooHop.simulate(kangaroos[0], kangaroos[1]);
        String result = KangarooHop.kangaroosWillMeet(kangaroos[0], kangaroos[1]);

        assertThat(result).isEqualTo(EXPECTED);
    }


    public static KangarooHop.RacingKangaroo[] createKangaroos(int[] input) {
        KangarooHop.RacingKangaroo k1 = new KangarooHop.RacingKangaroo(input[0], input[1], 0);
        KangarooHop.RacingKangaroo k2 = new KangarooHop.RacingKangaroo(input[2], input[3], 1);
        return new KangarooHop.RacingKangaroo[]{k1, k2};
    }

    public static int[] parseStringInput(String input) {
        String[] sInput = input.split(" ");
        return Arrays.stream(sInput).mapToInt(Integer::new).toArray();
    }
}