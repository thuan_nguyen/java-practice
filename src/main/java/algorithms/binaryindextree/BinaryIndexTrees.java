package algorithms.binaryindextree;

import lambdas.Add;
import lambdas.Operator;
import lambdas.Xor;

import java.util.Arrays;
import java.util.stream.IntStream;

import static utilities.MyUtilities.printIntArray;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class BinaryIndexTrees {
    private int[] original;
    private int[] bitXor;
    private int[] bitSum;
    private final Operator<Integer> XOR = new Xor();
    private final Operator<Integer> ADD = new Add();

    public BinaryIndexTrees(int[] array) {
        this.original = Arrays.copyOf(array, array.length);
        initializeBitXor(array);
        initializeBitSum(array);
    }

    private void initializeBitXor(int[] array){
        this.bitXor = new int[array.length];
        IntStream.range(0, array.length).forEach(i -> updateXor(i, array[i]));
    }

    private void initializeBitSum(int[] array) {
        this.bitSum = new int[array.length];
        IntStream.range(0, array.length).forEach(i -> updateSum(i, array[i]));
    }

    public void update(int index, int value) {
        updateXor(index, value);
        updateSum(index, value);
    }

    public void update(int[] array, int index, int value, Operator<Integer> operator) {
        index += 1;
        while(index <= array.length) {
            array[index - 1] = operator.operate(array[index - 1], value);
            index += index & -index;
        }
    }

    public int reduce(int[] array, int end, Operator<Integer> operator) {
        if(end >= array.length || end < 0) throw new IndexOutOfBoundsException("OUT OF RANGE");
        int index = end + 1;
        int sum = array[index - 1];
        index -= index & -index;

        while(index >= 1) {
            sum = operator.operate(sum, array[index - 1]);
            index -= index & -index;
        }

        return sum;
    }

    public int sumTotal(int end) throws Exception {
        int[] array = bitSum;
        return reduce(array, end, ADD);
    }

    public int sumRange(int start, int end) throws Exception {
        if(start - 1 == -1) return sumTotal(end);
        return sumTotal(end) - sumTotal(start - 1);
    }


    public int xorTotal(int end) {
        int[] array = bitXor;
        return reduce(array, end, XOR);
    }

    public int xorRange(int start, int end) throws Exception {
        if(start - 1 == -1) return xorTotal(end);
        return xorTotal(end) - xorTotal(start - 1);
    }

    public void updateXor(int index, int value) {
        update(bitXor, index, value, XOR);
    }

    public void updateSum(int index, int value) {
        update(bitSum, index, value, ADD);
    }

    public void printOriginal() {
        printIntArray(original);
    }

    public void printBitXor() {
        printIntArray(bitXor);
    }

    public void printBitSum() {
        printIntArray(bitSum);
    }


}
