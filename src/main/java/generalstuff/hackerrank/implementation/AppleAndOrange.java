package generalstuff.hackerrank.implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class AppleAndOrange {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int t = in.nextInt();
        int a = in.nextInt();
        int b = in.nextInt();
        int m = in.nextInt();
        int n = in.nextInt();

        int[] apple = new int[m];
        int[] orange = new int[n];

        for(int apple_i=0; apple_i < m; apple_i++){
            apple[apple_i] = in.nextInt();
        }

        for(int orange_i=0; orange_i < n; orange_i++){
            orange[orange_i] = in.nextInt();
        }

        House house = new House(s, t);
        FruitTree appleTree = new FruitTree("apple", a);
        FruitTree orangeTree = new FruitTree("orange", b);

        Map<String, FruitTree> trees = new HashMap<>();
        trees.put("apple", appleTree);
        trees.put("orange", orangeTree);

        Yard yard = new Yard(house, trees);
        printFruitsOnHouse(yard, "apple", apple);
        printFruitsOnHouse(yard, "orange", orange);
    }

    public static void printFruitsOnHouse(Yard yard, String treeType, int[] fruits) {
        try {
            int numberOfFruitsOnHouse = yard.numberOfFruitsOnHouse(treeType, fruits);
            System.out.println(numberOfFruitsOnHouse);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }


    public static class Yard {
        private final House house;
        private final Map<String, FruitTree> trees;

        public Yard(House house, Map<String, FruitTree> trees) {
            this.house = house;
            this.trees = trees;
        }

        public int numberOfFruitsOnHouse(String treeType, int[] fruits) throws Exception {
            int sum = 0;
            FruitTree tree = trees.get(treeType);
            if(tree != null) {
                for(int fruit : fruits) {
                    int position = trees.get(treeType).fruitPosition(fruit);
                    if (fruitIsOnHouse(position)) ++sum;
                }
            } else {
                throw new Exception("FruitTree: " + treeType + " not found");
            }
            return sum;
        }

        public boolean fruitIsOnHouse(int fruitPosition) {
            return (fruitPosition >= house.getHouseStart())
                    && (fruitPosition <= house.getHouseEnd());
        }
    }

    public static class House {
        private final int houseStart;
        private final int houseEnd;

        public House(int houseStart, int houseEnd) {
            this.houseStart = houseStart;
            this.houseEnd = houseEnd;
        }

        public int getHouseStart() {
            return houseStart;
        }

        public int getHouseEnd() {
            return houseEnd;
        }
    }

    public static class FruitTree {
        private final String type;
        private int position;

        public FruitTree(String type, int position) {
            this.type = type;
            this.position = position;
        }

        public String getType() {
            return type;
        }

        public int getPosition() {
            return position;
        }

        public int fruitPosition(int distanceFromTree) {
            return position + distanceFromTree;
        }
    }
}
