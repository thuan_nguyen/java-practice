package generalstuff.hackerrank.thirtydaytrial;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Author: Tony Nguyen on 28/05/17.
 */
public class MapsAndDictionaries {
    public static void main(String []args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        PhoneBook phoneBook = new PhoneBook();
        for(int i = 0; i < n; i++){
            String name = in.next();
            String phone = in.next();
            phoneBook.addContact(name, phone);
            // Write code here
        }

        String[] names = new String[n];
        while(in.hasNext()){
            String s = in.next();
            phoneBook.printContactWithName(s);
        }
        in.close();
    }

    public static class PhoneBook {
        private Map<String, String> contacts;
        public PhoneBook(List<Contact> contacts) {
            this.contacts = new HashMap<>();
            contacts.forEach(contact -> this.contacts.putIfAbsent(contact.getName(), contact.getNumber()));
        }

        public PhoneBook() {
            contacts = new HashMap<>();
        }

        public String addContact(String name, String number) {
            return contacts.putIfAbsent(name, number);
        }

        public Stream getContacts(String[] names) {
            return Stream.of(names)
                    .filter(name -> contacts.containsKey(name));
        }

        public void printContactsWithNames(String[] names) {
            StringBuilder builder = new StringBuilder();

            Stream matchingNames = getContacts(names);
            matchingNames.forEach(name -> {
                builder.append(name);
                builder.append(" ");
                builder.append(contacts.get(name));
                builder.append("\n");
            });

            System.out.println(builder.toString());
        }

        public void printContactWithName(String name) {
            StringBuilder builder = new StringBuilder();
            if(contacts.containsKey(name)) {
                builder.append(name);
                builder.append("=");
                builder.append(contacts.get(name));
            } else {
                builder.append("Not found");
            }

            System.out.println(builder.toString());
        }
    }

    public static class Contact {
        private String name;
        private String number;

        public Contact(String name) {
            this(name, null);
        }

        public Contact(String name, String number) {
            this.name = name;
            this.number = number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Contact)) return false;

            Contact contact = (Contact) o;

            if (name != null ? !name.equals(contact.name) : contact.name != null) return false;
            return number != null ? number.equals(contact.number) : contact.number == null;
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }

        public String getName() {
            return name;
        }

        public String getNumber() {
            return number;
        }
    }
}
