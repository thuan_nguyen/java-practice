package primitives;

/**
 * Created by Tony Nguyen on 17/05/17.
 */
public class CharAndBooleans {
    //https://unicode-table.com/en/#00AE
    public static char REGISTER_SYMBOL = '\u00AE';

    public static void main(String[] args) {
        System.out.println(REGISTER_SYMBOL);
        System.out.println(true);
        System.out.println(false);
    }
}
