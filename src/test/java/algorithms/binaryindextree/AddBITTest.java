package algorithms.binaryindextree;

import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Author: Tony Nguyen on 5/06/17.
 */
public class AddBITTest {
    private final static int[] DEFAULT_INPUT = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private BinaryIndexTree<Integer> BIT;

    @Before
    public void before() {
        Integer[] input = IntStream.of(DEFAULT_INPUT).boxed().toArray(Integer[]::new);
        BIT = new AddBIT(input);
    }

    @Test
    public void fold_CanSumFromStart() throws Exception {
        int EXPECTED = 1;
        int result = BIT.fold(0);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void fold_CanSumToMiddle() throws Exception {
        int EXPECTED = 21;
        int result = BIT.fold(5);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void fold_CanSumToEnd() throws Exception {
        int EXPECTED = 55;
        int result = BIT.fold(9);

        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void constructor_initializesCorrectly() throws Exception {
        String EXPECTED = "1, 3, 3, 10, 5, 11, 7, 36, 9, 19";
        Integer[] input = IntStream.of(DEFAULT_INPUT).boxed().toArray(Integer[]::new);
        BinaryIndexTree<Integer> bit = new AddBIT(input);

        String result = bit.bitToString();
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void constructor_initializesCorrectly_withReverseInput() throws Exception {
        String EXPECTED = "10, 19, 8, 34, 6, 11, 4, 52, 2, 3";
        final int[] REVERSED_INPUT = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        Integer[] input = IntStream.of(REVERSED_INPUT).boxed().toArray(Integer[]::new);
        BIT = new AddBIT(input);

        String result = BIT.bitToString();
        assertThat(result).isEqualTo(EXPECTED);
        System.out.println(BIT);
    }

    @Test
    public void foldRange_sumsRange_nearBeginning() {
        final int EXPECTED = 21;
        final int LOWER = 0, UPPER = 5;
        int sum = BIT.foldRange(LOWER, UPPER);

        assertThat(sum).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_sumsRange_1awayNearBeginning() {
        final int EXPECTED = 20;
        final int LOWER = 1, UPPER = 5;
        int sum = BIT.foldRange(LOWER, UPPER);

        assertThat(sum).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_sumsRange_nearMiddle() {
        final int EXPECTED = 22;
        final int LOWER = 3, UPPER = 6;
        int sum = BIT.foldRange(LOWER, UPPER);

        assertThat(sum).isEqualTo(EXPECTED);
    }


    @Test
    public void foldRange_sumsRange_nearEnd() {
        final int EXPECTED = 27;
        final int LOWER = 7, UPPER = 9;
        int sum = BIT.foldRange(LOWER, UPPER);

        assertThat(sum).isEqualTo(EXPECTED);
    }

    @Test
    public void foldRange_sumsRange_1awayNearEnd() {
        final int EXPECTED = 24;
        final int LOWER = 6, UPPER = 8;
        int sum = BIT.foldRange(LOWER, UPPER);

        assertThat(sum).isEqualTo(EXPECTED);
    }
}