package generalstuff.hackerrank.bitmanipulation;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class CipherTest {
    private final String DEFAULT_BINARY = "1001010";
    private final int DEFAULT_K = 4;

    private String encoded;

    @Before
    public void before() {
        encoded = Cipher.encode(DEFAULT_BINARY, DEFAULT_K);
    }

    @Test
    public void encode() throws Exception {
        final String EXPECTED = "1110100110";
        assertThat(encoded).isEqualTo(EXPECTED);
    }

    @Test
    public void binaryStringToBytes() throws Exception {
    }

    @Test
    public void decode_withDefault() throws Exception {
        String result = Cipher.decode(encoded, DEFAULT_K);
        assertThat(result).isEqualTo(DEFAULT_BINARY);

        result = Cipher.decode(encoded, DEFAULT_BINARY.length(), DEFAULT_K);
        assertThat(result).isEqualTo(DEFAULT_BINARY);
    }

    @Test
    public void decode_withCustom_2OnesOnEnd() throws Exception {
        String INPUT = "1001011";
        String ENCODED = "111110001";
        final int K = 3;

        encoded = Cipher.encode(INPUT, K);
        assertThat(encoded).isEqualTo(ENCODED);

        String result = Cipher.decode(encoded, K);
        assertThat(result).isEqualTo(INPUT);

        result = Cipher.decode(encoded, INPUT.length(), K);
        assertThat(result).isEqualTo(INPUT);
    }

    @Test
    public void xorRange_outputs0_0Start() {
        int[] INPUT = {0, 1, 1, 1, 1};
        int EXPECTED = 0;

        int result = Cipher.xorRange(INPUT, 0, INPUT.length - 1);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void xorRange_outputs0_0End() {
        int[] INPUT = {1, 1, 1, 1, 0};
        int EXPECTED = 0;

        int result = Cipher.xorRange(INPUT, 0, INPUT.length - 1);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void xorRange_outputs1_1End() {
        int[] INPUT = {0, 0, 0, 0, 1};
        int EXPECTED = 1;

        int result = Cipher.xorRange(INPUT, 0, INPUT.length - 1);
        assertThat(result).isEqualTo(EXPECTED);
    }

    @Test
    public void xorRange_correctOutput_1Start() {
        int[] INPUT = {1, 0, 0, 0, 0};
        int EXPECTED = 1;

        int result = Cipher.xorRange(INPUT, 0, INPUT.length - 1);
        assertThat(result).isEqualTo(EXPECTED);
    }
}