package generalstuff.hackerrank.warmup;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class Staircase {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        printStairCase(n);
    }

    public static void printStairCase(int n) {
        IntStream.rangeClosed(1, n).forEach(
                (i) -> {
                    printSpaces(n - i);
                    printCharacters(i, '#');
                    System.out.println();
                }
        );
    }

    public static void printSpaces(int n) {
        printCharacters(n, ' ');
    }

    public static void printCharacters(int n, Character symbol) {
        IntStream.range(0, n).forEach((i) -> System.out.print(symbol));
    }
}
