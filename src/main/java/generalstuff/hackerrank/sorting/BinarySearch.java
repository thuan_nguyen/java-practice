package generalstuff.hackerrank.sorting;

/**
 * Created by Tony Nguyen on 23/05/17.
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] INPUT = {1, 2, 3, 4, 5, 6, 7};
        int index = binarySearch(INPUT, 8);
        System.out.println(index);
    }

    public static int binarySearch(int[] array, int numberToSearch) {
        int i = 0;
        int j = array.length;

        while(i < j) {
            int p = i + (j - i)/2;
            if(numberToSearch < array[p]) j = p - 1;
            if(numberToSearch > array[p]) i = p + 1;

            if(numberToSearch == array[p]) return p;
        }

        return -1;
    }
}
