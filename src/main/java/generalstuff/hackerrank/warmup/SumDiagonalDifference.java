package generalstuff.hackerrank.warmup;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class SumDiagonalDifference {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixSize = scanner.nextInt();

        int[][] matrix = new int[matrixSize][matrixSize];
        for(int row = 0; row < matrix.length; row++) {
            for(int col = 0; col < matrix[1].length; col++) {
                matrix[row][col] = scanner.nextInt();
            }
        }
//        System.out.println(matrix.length);
//        System.out.println(matrix[1].length);
    }

    public static int differenceOfSumOfDiagonals(int[][] matrix) throws Exception {
        if (matrix.length != matrix[0].length) throw new Exception("matrix is not square".toUpperCase());
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            sum += matrix[i][i] - matrix[(matrix.length-1) - i][i];
        }
        return Math.abs(sum);
    }
}
