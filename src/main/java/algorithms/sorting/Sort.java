package algorithms.sorting;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public interface Sort<T> {
    void sort(List<T> values, Comparator<T> comparator);
    void reverse(List<T> values, Comparator<T> comparator);
}
