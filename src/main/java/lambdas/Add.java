package lambdas;

/**
 * Created by Tony Nguyen on 4/06/17.
 */
public class Add implements Operator<Integer> {
    @Override
    public Integer operate(Integer a, Integer b) {
        return a + b;
    }
}
