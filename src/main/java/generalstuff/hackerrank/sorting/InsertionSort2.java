package generalstuff.hackerrank.sorting;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class InsertionSort2 {

    public static void insertIntoSorted(int[] array) {
        int shiftCount = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                int j = i + 1;
                int focus = array[j];
                while (focus < array[j - 1]) {
                    array[j] = array[j - 1];
                    ++shiftCount;
                    if(--j <= 0) break;
                }
                array[j] = focus;
            }
            printArray(array);
        }
        System.out.println(shiftCount);
    }


    /* Tail starts here */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int[] numbers = new int[s];
        for (int i = 0; i < s; i++) {
            numbers[i] = in.nextInt();
        }
//        int[] numbers = {2, 1, 3};
//        int[] numbers = {10, 9, 4, 6, 8, 1};
//        int[] numbers = {2, 1, 3, 1, 2};
        insertIntoSorted(numbers);
    }


    private static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
        System.out.println("");
    }
}
