package algorithms.binaryindextree;

import lambdas.Add;
import java.util.stream.IntStream;

/**
 * Author: Tony Nguyen on 4/06/17.
 */
public class AddBIT extends AbstractBinaryIndexTree<Integer> {
    protected AddBIT(Integer[] array) {
        super(array, new Add());
    }

    @Override
    protected Integer[] initialize(Integer[] array) {
        int[] arr = new int[array.length];
        return IntStream.of(arr).boxed().toArray(Integer[]::new);
    }

    @Override
    public Integer foldRange(int start, int end) {
        if(start - 1 == -1) return fold(end);
        return fold(end) - fold(start - 1);
    }
}
