package generalstuff.hackerrank.crackingthecodinginterview;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class LeftRotation {
    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int n = in.nextInt();
//        int k = in.nextInt();
//        int a[] = new int[n];
//        for(int a_i=0; a_i < n; a_i++){
//            a[a_i] = in.nextInt();
//        }

        int[] a = {1, 2, 3, 4, 5};
//        shiftLeftWrapAround(a, 4);
        printArray(shiftLeftWrapAround(a, 4));
    }

    public static int[] shiftLeftWrapAround(int[] input, int rotations) {
        int[] result = new int[input.length];

        int newZeroIndexPosition = (input.length - rotations) % input.length;

        for(int i = newZeroIndexPosition; i < input.length; i++) {
            result[i] = input[i - newZeroIndexPosition];
        }

        for(int i = 0; i < newZeroIndexPosition; i++) {
            result[i] = input[input.length - (newZeroIndexPosition - i)];
        }

        return result;
    }

    public static void swap(int[] input, int p1, int p2) {
        int temp = input[p1];
        input[p1] = input[p2];
        input[p2] = temp;
    }

    public static void printArray(int[] input) {
        for(int i = 0; i < input.length - 1; i++) {
            System.out.print(input[i] + " ");
        }
        System.out.print(input[input.length - 1]);
    }
}
