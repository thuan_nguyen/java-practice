package generalstuff.hackerrank.warmup;

import java.util.Scanner;
import java.util.stream.LongStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class BigSum {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        long arr[] = new long[n];
        for(int i = 0; i < n; i++){
            arr[i] = in.nextLong();
        }

        long sum = sum(arr);
        System.out.println(sum);
    }

    public static long sum(long[] numbers) {
        return LongStream.of(numbers).sum();
    }
}
