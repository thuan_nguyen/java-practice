package generalstuff.hackerrank.warmup;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class BirthdayCakeCandles {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Map<Integer, Integer> candles = new HashMap<>();

        IntStream.range(0, n).forEach((i) -> {
            int candleHeight = scanner.nextInt();
            candles.computeIfPresent(candleHeight, (key, value) -> value + 1);
            candles.putIfAbsent(candleHeight, 1);
        });

        int tallestCandle = candles.values().stream().reduce(Integer::max).get();
        System.out.println(tallestCandle);
    }
}
