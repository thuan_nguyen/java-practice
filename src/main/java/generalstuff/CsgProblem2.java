package generalstuff;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Tony Nguyen on 21/05/17.
 */
public class CsgProblem2 {
    public static void main(String[] args) throws IOException {
        String line;
        Scanner scanner = new Scanner(System.in);
        while (!(line = scanner.nextLine()).isEmpty()) {

        }
        System.out.println("Good bye");
    }

    public class Ellipse {
        private double a;
        private double b;
        private double x;
        private double y;

        public Ellipse(double x, double y, double a, double b) {
            this.x = x;
            this.y = y;

            this.a = a;
            this.b = b;
        }

        public boolean isPointWithinEllipse(double x, double y){
            if(!isXPointWithinEllipseWidth(x)) return false;
            if(!isYPointWithinEllipseHeight(y)) return false;

            return false;
        }

        private boolean isXPointWithinEllipseWidth(double x) {
            return (x <= (this.x + a)) && (x >= (this.x - a));
        }

        private boolean isYPointWithinEllipseHeight(double y) {
            return (y <= (this.y + b)) && (y >= (this.y - b));
        }
    }
}

