package generalstuff.hackerrank.thirtydaytrial;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class CloudJump {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int c[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }

        int energy = remainingEnergy(c, n, k);
        System.out.println(energy);
    }

    public static int remainingEnergy(int[] clouds, int n, int k) {
        int energy = 100;
        int currentPosition = 0;
        while(energy >= 0) {
            currentPosition = (currentPosition + k) % n;
            if (clouds[currentPosition] > 0) energy -= 2;
            --energy;
            if (currentPosition == 0) break;
        }
        return energy;
    }
}