package generalstuff.hackerrank.thirtydaytrial;

import java.util.Scanner;

/**
 * Created by Tony Nguyen on 22/05/17.
 */
public class DataTypes {
    public static void main(String[] args) {
        int i = 4;
        double d = 4.0;
        String s = "HackerRank ";

        Scanner scan = new Scanner(System.in);
                /* Declare second integer, double, and String variables. */
        int i2 = Integer.parseInt(scan.nextLine());
        double d2 = Double.parseDouble(scan.nextLine());
        String s2 = scan.nextLine();


        /* Read and save an integer, double, and String to your variables.*/
        // Note: If you have trouble reading the entire String, please go back and review the Tutorial closely.

        /* Print the sum of both integer variables on a new line. */
        int sumInts = i + i2;
        System.out.println(sumInts);

        /* Print the sum of the double variables on a new line. */
        double sumDoubles = d + d2;
        System.out.println(sumDoubles);

        /* Concatenate and print the String variables on a new line;
        	the 's' variable above should be printed first. */
        String concatenatedStrings = s + s2;
        System.out.println(concatenatedStrings);
        scan.close();
    }
}
