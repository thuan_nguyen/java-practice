package generalstuff.hackerrank.implementation;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static generalstuff.hackerrank.implementation.AppleAndOrange.printFruitsOnHouse;

/**
 * Created by Tony Nguyen on 25/05/17.
 */
public class AppleAndOrangeTest {
    @Test
    public void basicTest() {
        int s = 7, t = 11;
        int a = 5, b = 15;
        int m = 3, n = 2;

        int[] apple = {-2, 2, 1};
        int[] orange = {5, -6};

        AppleAndOrange.House house = new AppleAndOrange.House(s, t);
        AppleAndOrange.FruitTree appleTree = new AppleAndOrange.FruitTree("apple", a);
        AppleAndOrange.FruitTree orangeTree = new AppleAndOrange.FruitTree("orange", b);

        Map<String, AppleAndOrange.FruitTree> trees = new HashMap<>();
        trees.put("apple", appleTree);
        trees.put("orange", orangeTree);

        AppleAndOrange.Yard yard = new AppleAndOrange.Yard(house, trees);
        printFruitsOnHouse(yard, "apple", apple);
        printFruitsOnHouse(yard, "orange", orange);
    }
}